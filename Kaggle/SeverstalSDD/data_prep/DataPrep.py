import os
import pandas as pd
from shutil import copyfile, rmtree
import math
from PIL import Image


def data_preparation():
    y_train = pd.read_csv('data/orig_data/train.csv')
    data = pd.DataFrame(list(y_train['ImageId_ClassId'].str.split('_')[:]), columns=['ImageId', 'ClassId'])
    data['EncodedPixels'] = y_train['EncodedPixels']
    data['ClassId'] = data.apply(lambda x: -1 if pd.isna(x['EncodedPixels']) else x['ClassId'], axis=1)
    data.to_csv('orig_data.csv')
    return data


def sort_data():
    rmtree(f'orig_data/train_data/') if 'train_data' in os.listdir('orig_data') else None
    [os.makedirs(f'{os.getcwd()}/orig_data/train_data/{i}') for i in [-1, 1, 2, 3, 4]]

    data = pd.read_csv('orig_data.csv')
    all_images = data.ImageId.unique()
    data = data.dropna()
    non_defects_img = [i for i in all_images if i not in data.ImageId.unique()]
    data.apply(lambda x: copyfile(f'orig_data/orig_data/train_images/{x["ImageId"]}',
                                  f'orig_data/train_data/{x["ClassId"]}/{x["ImageId"]}'), axis=1)
    [copyfile(f'orig_data/orig_data/train_images/{i}', f'orig_data/train_data/-1/{i}') for i in non_defects_img]


def create_data_l():
    for i in [-1, 1, 2, 3, 4]:
        cls_dir = os.listdir(f'orig_data/train_data/{i}')
        print(cls_dir)


def create_class_weight():
    labels_dict = {}
    for i in [-1, 1, 2, 3, 4]:
        labels_dict[str(i)] = len(os.listdir(f'orig_data/train_data/{i}'))

    total = sum(labels_dict.values())
    keys = labels_dict.keys()
    class_weight = dict()

    for key in keys:
        score = math.log(0.15*total/float(labels_dict[key]))
        class_weight[key] = score if score > 1.0 else 1.0
    return class_weight


def reversed_image_creation(cls):
    images = os.listdir(f'orig_data/train_data/{cls}')
    for img_name in images:
        img = Image.open(f'orig_data/train_data/{cls}/{img_name}')
        mirrored_image = img.transpose(Image.FLIP_LEFT_RIGHT)
        mirrored_image.save(f'orig_data/transformed_data/{cls}/mr_{img_name}')

    print(len(os.listdir(f'orig_data/transformed_data/{cls}')))


def get_multi_class_images():
    data = pd.read_csv("data/orig_data/train.csv").dropna()
    spl = data['ImageId_ClassId'].str.split('_', expand=True)
    data['Img'], data['Cls'] = spl[0], spl[1]
    data.drop('ImageId_ClassId', axis=1)
    images = {}
    for i in data.groupby('Img'):
        if len(i[1]['EncodedPixels']) > 1:
            images.update({i[0]: list(i[1]['Cls'])})


if __name__ == '__main__':
    pass
