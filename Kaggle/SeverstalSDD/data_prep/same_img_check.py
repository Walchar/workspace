import os
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


path_to_train_data = 'data/orig_data/train_images/'
path_to_train_csv = 'data/orig_data/train.csv'


class SameImageCheck(object):
    def __init__(self):
        self.images = pd.read_csv('data.csv')
        self.train = pd.read_csv(path_to_train_csv)
        self.train = pd.concat([self.train, pd.DataFrame(list(self.train['ImageId_ClassId'].str.split('_')),
                                                         columns=['ImageId', 'ClassId'])], axis=1)
        self.train = self.train.drop('ImageId_ClassId', axis=1).dropna()
        self.train = self.create_data_dict(self.train)
        _, self.plt = plt.subplots(2, figsize=(20, 20))

    def check_images(self, images):
        if images[0] in self.train.keys():
            mask = self.calc_cords(self.train[images[0]][0], 256)
            self.display_one(mask, images[0], 0, self.train[images[0]][1])
        else:
            img = mpimg.imread(path_to_train_data + images[0])
            self.plt[0].imshow(img)
            self.plt[0].set_title(images[0] + '_1' + f'_class=-1')

        if images[1] in self.train.keys():
            mask = self.calc_cords(self.train[images[1]][0], 256)
            self.display_one(mask, images[1], 1, self.train[images[1]][1])
        else:
            img = mpimg.imread(path_to_train_data + images[1])
            self.plt[1].imshow(img)
            self.plt[1].set_title(images[1] + '_2' + f'_class=-1')

        fig = plt.gcf()
        fig.set_size_inches(18.5, 10.5)
        fig.savefig(f'same_images/{images[0]}__{images[1]}', dpi=100)
        plt.close()
        _, self.plt = plt.subplots(2, figsize=(20, 20))
        # self.clean(images)

    def clean(self, images):
        inp = input('images for deletion: ')
        inp = list(inp)
        for i in inp:
            os.remove(f'/home/user/Projects/Severstal SDD/data/train_images v1.1/{images[int(i) - 1]}')

    def calc_cords(self, pix: str, height):
        elements = [int(i) for i in pix.split(' ')]
        start_pos = elements[::2]
        count = elements[1::2]
        cords = {'x': [], 'y': []}
        for i in range(len(start_pos)):
            for j in range(count[i]):
                pixel = start_pos[i] + j
                cords['x'].append(pixel // height)
                cords['y'].append(pixel % height)
        return cords['x'], cords['y']

    def display_one(self, mask, name, p, c):
        color = ['red', 'yellow', 'blue', 'orange']
        img = mpimg.imread(f'data/orig_data/train_images/{name}')
        self.plt[p].imshow(img)
        self.plt[p].scatter(*mask, s=0.001, c=color[int(c) - 1])
        self.plt[p].set_title(name + f'_{p + 1}' + f'_class={c}')

    @staticmethod
    def create_data_dict(train_data: pd.DataFrame):
        data = dict()
        for i in range(len(train_data.index)):
            data[train_data.iloc[i]['ImageId']] = [train_data.iloc[i]['EncodedPixels'], train_data.iloc[i]['ClassId']]
        return data

    def run(self):
        for i in self.images.index:
            self.check_images(self.images.iloc[i])


def train_file_update():
    from PIL import Image

    train = pd.read_csv('data/transformed_data/version 1.1 (removed_same_images)/train.csv')
    train['ImageId'] = train['ImageId_ClassId'].str.split('_', n=1, expand=True)[0]
    for i in train['ImageId'].unique():
        try:
            Image.open(f'data/transformed_data/version 1.1 (removed_same_images)/train_images/{i}')
        except FileNotFoundError:
            print('exc')
            train = train[train.ImageId != i]

    train = train.drop('ImageId', axis=1)
    train.to_csv('data/transformed_data/version 1.1 (removed_same_images)/new_train.csv', index=False)


if __name__ == '__main__':
    obj = SameImageCheck()
    obj.run()
