import os
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


class ImageCheck(object):
    def __init__(self):
        self.train = pd.read_csv('data/transformed_data/version 1.1/train.csv').dropna()
        spl_col = self.train['ImageId_ClassId'].str.split('_', expand=True)
        self.train['ImageId'], self.train['ClassId'] = spl_col[0], spl_col[1]
        self.train = self.train.drop('ImageId_ClassId', axis=1)
        self.train = self.train.sort_values(by='ClassId')

    def check_images(self):
        for i in self.train.index:
            row = self.train.loc[i]
            mask = self.calc_cords(row['EncodedPixels'], 256)
            self.display_two(mask, row['ImageId'], row['ClassId'])
            plt.show()
            plt.close()

    @staticmethod
    def display_two(mask, name, c):
        _, fig = plt.subplots(2, figsize=(20, 20))
        color = ['red', 'yellow', 'blue', 'orange']
        img = mpimg.imread(f'data/orig_data/train_images/{name}')
        fig[0].imshow(img)
        fig[0].set_title(name + f'_class={c} with_mask')
        fig[0].scatter(*mask, s=0.001, c=color[int(c) - 1])
        fig[1].imshow(img)
        fig[1].set_title(name + f'_class={c} without_mask')

    @staticmethod
    def calc_cords(pix: str, height):
        elements = [int(i) for i in pix.split(' ')]
        start_pos = elements[::2]
        count = elements[1::2]
        cords = {'x': [], 'y': []}
        for i in range(len(start_pos)):
            for j in range(count[i]):
                pixel = start_pos[i] + j
                cords['x'].append(pixel // height)
                cords['y'].append(pixel % height)
        print(cords['x'])
        print(cords['y'])
        return cords['x'], cords['y']

    def run(self):
        self.check_images()


if __name__ == '__main__':
    obj = ImageCheck()
    obj.run()
