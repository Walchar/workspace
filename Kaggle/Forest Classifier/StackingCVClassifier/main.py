from preparation import Preparation
import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from mlxtend.classifier import StackingCVClassifier
from sklearn.ensemble import GradientBoostingClassifier


class Model(object):
    def __init__(self, x_train, y_train, kaggle_test):
        self.X_train = x_train
        self.y_train = y_train
        self.kaggle_test = kaggle_test

    def __fit(self):
        x_train = self.X_train
        y_train = self.y_train

        random = 42
        ab_clf = AdaBoostClassifier(n_estimators=500,
                                    base_estimator=DecisionTreeClassifier(min_samples_leaf=2, random_state=random),
                                    random_state=random)

        gb_clf = GradientBoostingClassifier(n_estimators=500, max_features=0.3, max_depth=14, min_samples_split=2,
                                            min_samples_leaf=1, verbose=1, random_state=random)

        rf_clf = RandomForestClassifier(n_estimators=500, max_features=0.3, max_depth=14, min_samples_split=2,
                                        min_samples_leaf=1, verbose=1, random_state=random)

        stack = StackingCVClassifier(classifiers=[ab_clf, gb_clf, rf_clf], meta_classifier=gb_clf, cv=5,
                                     use_probas=True, use_features_in_secondary=True, verbose=1,
                                     random_state=random)

        stack.fit(x_train, y_train)
        y_pred = stack.predict(self.kaggle_test)

        for i in range(1, 8):
            print(f'Count of class {i} in prediction = ', list(y_pred).count(i))
        pd.DataFrame(data={'Id': obj.kt_id, 'Cover_Type': y_pred}).to_csv('submission.csv', index=False)

    def run(self):
        self.__fit()


if __name__ == '__main__':
    obj = Preparation()
    model = Model(*obj.get_data())
    model.run()
