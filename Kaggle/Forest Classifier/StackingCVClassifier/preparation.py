from sklearn.preprocessing import StandardScaler
import pandas as pd
from sklearn.mixture import GaussianMixture

zero_important_features = ['Id', 'Soil_Type7', 'Soil_Type8', 'Soil_Type9', 'Soil_Type14', 'Soil_Type15', 'Soil_Type16',
                           'Soil_Type18', 'Soil_Type19', 'Soil_Type21', 'Soil_Type25', 'Soil_Type26', 'Soil_Type27',
                           'Soil_Type28', 'Soil_Type34', 'Soil_Type36', 'Soil_Type37']

stoneyness = [4, 3, 1, 1, 1, 2, 0, 0, 3, 1, 1, 2, 1, 0, 0, 0, 0, 3, 0, 0,
              0, 4, 0, 4, 4, 3, 4, 4, 4, 4, 4, 4, 4, 4, 1, 4, 4, 4, 4, 4]


class Preparation(object):
    def __init__(self):
        self.X_train, self.y_train, self.kaggle_test, self.kt_id = self.__load_data()
        self.scaler = StandardScaler()

    @staticmethod
    def __create_new_cols(data: pd.DataFrame):
        data['Hydro_Elevation_diff'] = data['Elevation'] - data['Vertical_Distance_To_Hydrology']
        data['Hydro_Euclidean'] = (data['Horizontal_Distance_To_Hydrology'] ** 2 +
                                   data['Vertical_Distance_To_Hydrology'] ** 2)
        data['Hydro_Euclidean'] = data['Hydro_Euclidean'] ** 0.5
        data['Hydro_Fire_sum'] = data['Horizontal_Distance_To_Hydrology'] + data['Horizontal_Distance_To_Fire_Points']
        data['Hydro_Fire_diff'] = abs(data['Horizontal_Distance_To_Hydrology'] -
                                      data['Horizontal_Distance_To_Fire_Points'])
        data['Hydro_Road_sum'] = data['Horizontal_Distance_To_Hydrology'] + data['Horizontal_Distance_To_Roadways']
        data['Hydro_Road_diff'] = abs(data['Horizontal_Distance_To_Hydrology'] -
                                      data['Horizontal_Distance_To_Roadways'])
        data['Road_Fire_sum'] = data['Horizontal_Distance_To_Roadways'] + data['Horizontal_Distance_To_Fire_Points']
        data['Road_Fire_diff'] = abs(data['Horizontal_Distance_To_Roadways'] -
                                     data['Horizontal_Distance_To_Fire_Points'])
        data['Stoneyness'] = sum(i * data['Soil_Type{}'.format(i)] for i in range(1, 41))
        data['Stoneyness'] = data['Stoneyness'].replace(range(1, 41), stoneyness)
        return data

    def __load_data(self):
        train = self.__create_new_cols(pd.read_csv('data/train.csv'))
        kaggle_test = self.__create_new_cols(pd.read_csv('data/test.csv'))
        y_train, x_train = train['Cover_Type'], train.drop(['Cover_Type'], axis=1)
        kaggle_test_id = kaggle_test['Id']
        return x_train, y_train, kaggle_test, kaggle_test_id

    def __scaler(self):
        self.X_train = self.scaler.fit_transform(self.X_train)
        self.kaggle_test = self.scaler.transform(self.kaggle_test)

    def __gmm(self):
        gmix = GaussianMixture(n_components=10, verbose=1)
        gmix.fit(self.kaggle_test)
        self.X_train['Test_Cluster'] = gmix.predict(self.X_train)
        self.kaggle_test['Test_Cluster'] = gmix.predict(self.kaggle_test)

    def __drop(self):
        self.X_train = self.X_train.drop(zero_important_features, axis=1)
        self.kaggle_test = self.kaggle_test.drop(zero_important_features, axis=1)

    def get_data(self):
        self.__gmm()
        self.__drop()
        self.__scaler()
        return self.X_train, self.y_train, self.kaggle_test


