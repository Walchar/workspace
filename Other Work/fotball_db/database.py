from sqlalchemy import create_engine
import pandas as pd
import json
import re


class DataAddToDB(object):
    def __init__(self, historical, statistic):
        params = json.load(open('db_conf.json', 'r'))
        self.his = historical
        self.stat = statistic
        self.engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(
            params['user'], params['password'], params['host'], params['db']), pool_pre_ping=True)

    def add_historical(self):
        df = pd.DataFrame(self.his).drop('outcomes', axis=1)
        self.engine.execute("DROP TABLE historical_data;")
        df.to_sql(name='historical_data', con=self.engine, if_exists='replace')

    def add_statistic(self):
        data = []
        for key, value in self.stat.items():
            data.append(
                {"name": key, "three_o": value["3o"], "away": value["away"],  "home": value["home"], "wl": value["wl"],
                 "conceded": value["conceded"], "scored": value["scored"], "next_opponent": value["next"]["opponent"],
                 "next_venue": value["next"]["venue"]})
        df = pd.DataFrame(data)
        self.engine.execute("DROP TABLE statistic;")
        df.to_sql(name='statistic', con=self.engine, if_exists='replace')

    def run(self):
        self.add_historical()
        self.add_statistic()
