from keras.models import model_from_json

class Predict(object):
    def __init__(self):
        self.loss: str
        self.optimizer: str

    def __load_model(self):
        with open('model/model.json', 'r') as f:
            self.model = model_from_json(f.read())

    def __predict(self):
        self.model.predict()
