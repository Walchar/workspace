from concurrent.futures import ThreadPoolExecutor
from datetime import datetime, timezone
from os import listdir, getcwd
import pandas as pd
import numpy as np


def get_files_list():
    current_path = getcwd()
    files = []
    folder_path = f"{current_path}/data/"
    for elem in listdir(folder_path):
        files.append(folder_path + elem)
    return files


class Preparation(object):
    def __init__(self, files: list, n_sequences: int):
        self.files = files
        self.n_sequences = n_sequences
        self.data_frames = dict()
        self.reshaped_data = dict()

    def __dir__(self):
        return ['__delattr__', '__doc__', '__format__', '__hash__', '__init__', '__str__', 'run', 'get_data']

    def __data_preparation(self, file: str):
        res = dict()
        df = pd.read_csv(file, delimiter=',')
        if 'Open' == df.columns[1]:
            merge = lambda s1, s2: f"{s1} {s2}"
            df['Date'] = df['Date'].combine(df['Open'], merge)
            df = df.drop(['Open'], axis=1)
        df = df.rename(columns={'Date': 'Time'})
        df['Time'] = df['Time'].apply(self.__convert_date)
        res.update({file.split('/')[-1]: df})
        return res

    def __reshape(self, file, df):
        x_train = list()
        y_train = list()

        for index in df.index:
            if index + self.n_sequences + 1 > len(df.index):
                break
            x_train.append(np.array(df.iloc[index: index + self.n_sequences]))
            y_train.append(np.array(df.iloc[index + self.n_sequences]))

        self.reshaped_data.update({file: {'x': np.array(x_train), 'y': np.array(y_train)}})

    def run(self):
        with ThreadPoolExecutor(max_workers=len(self.files)) as executor:
            res = dict()
            [res.update(executor.submit(self.__data_preparation, file).result()) for file in self.files]

        self.data_frames = res
        for key, df in self.data_frames.items():
            self.__reshape(key, df)

    def get_data(self):
        return self.reshaped_data

    @staticmethod
    def __convert_date(date: str):
        date_format = "%d/%m/%Y %H:%M" if ':' in date else "%d/%m/%Y"
        dt = datetime.strptime(date, date_format)
        return dt.replace(tzinfo=timezone.utc).timestamp()


class Update(object):
    def __init__(self, files):
        self.files = files

    @staticmethod
    def appl_add_ema20(file):
            pass

    def run(self):
        for file in self.files:
            if 'AAPL_60.csv' in file:
                self.appl_add_ema20(file)
