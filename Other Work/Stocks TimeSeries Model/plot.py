import matplotlib.pyplot as plt
import pandas as pd

data = {'Date': [], 'High': []}
last_added = str()

df = pd.read_csv('data/AAPL_60.csv')

for index in df.index:
    date = str(df.iloc[index]['Date']).split('/')[2]
    if date != last_added:
        data['Date'].append(date)
        data['High'].append(df.iloc[index]['High'])
        last_added = date

plt.figure(num=None, figsize=(14, 8), dpi=80, facecolor='w', edgecolor='k')
plt.plot(data['Date'], data['High'])
plt.xlabel('Date')
plt.ylabel('Stock price')
plt.grid('on')
plt.savefig('apple.pdf')
