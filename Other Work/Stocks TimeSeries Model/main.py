from data_preparation import get_files_list
from data_preparation import Preparation, Update
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout
from sklearn.preprocessing import MinMaxScaler


class Model(object):
    def __init__(self, shaped_data: dict):

        self.scaler = MinMaxScaler()
        x, y = self.scaler.fit_transform(shaped_data['x'], shaped_data['y'])

        self.x, self.x_test, self.y, self.y_test = train_test_split(x, y, test_size=0.20)

    def __dir__(self):
        return ['__delattr__', '__doc__', '__format__', '__hash__', '__init__', '__str__', 'run']

    def __create_model(self):
        model = Sequential()
        model.add(LSTM(100, activation='relu', return_sequences=True, input_shape=(self.x.shape[1], self.x.shape[2])))
        model.add(Dropout(0.3))
        model.add(LSTM(70, activation='relu'))
        model.add(Dropout(0.2))
        model.add(Dense(self.x.shape[2]))
        model.compile(optimizer='adam', loss='mean_squared_error')
        self.model = model

    def __fit(self):
        self.model.fit(self.x, self.y, epochs=20, verbose=2)

    def __save(self):
        model_json = self.model.to_json()
        with open("model/model.json", "w") as json_file:
            json_file.write(model_json)

    def run(self):
        # self.__create_model()
        # self.__fit()
        # self.__save()
        from keras.models import model_from_json
        with open('model/model.json', 'r') as f:
            self.model = model_from_json(f.read())
        pred = self.model.predict(self.x_test)
        tr = self.y_test
        for i in range(len(tr)):
            print('p: ', ["%.4f" % elem for elem in pred[i]])
            print('t: ', ["%.4f" % elem for elem in tr[i]])
            print('==========')


if __name__ == '__main__':
    files = get_files_list()
    n_sequences = 20
    Update(files).run()
    obj = Preparation(files, n_sequences=n_sequences)
    obj.run()
    data = obj.get_data()
    for key, value in data.items():
        print('{} start training...'.format(key))
        Model(value).run()

