from requests_html import HTMLSession
import json
import dateparser
from telebot import TeleBot
import difflib
import time

API_KEY = 'iXj7F39xNqLWE9gF'
API_SECRET = 'LgNCvuG6MlDKooDHlXP10YzOpkK4u1Jf'
bot = TeleBot('954186222:AAFbjOwGRAUIyFoMgwp6y4S4qLQQHxGbrlo')


def create_start_point():
    try:
        with open('data/historical_data.json', 'r', encoding='utf-8') as f:
            data = json.load(f)
            return data, data[-1]['date']
    except:
        with open('data/historical_data.json', 'w', encoding='utf-8') as f:
            json.dump([], f, ensure_ascii=False, indent=2, sort_keys=False)
        date = dateparser.parse('1 year ago')
        return [], '{}-{}-{}'.format(date.year, date.month, date.day)


def read_data():
    session = HTMLSession()
    historical = list()
    fixed = list()
    inc = int()
    while True:
        r = session.get('http://livescore-api.com/api-client/scores/history.json?key={}&secret={}&from={}&page={}'
                        .format(API_KEY, API_SECRET, _from, inc))
        try:
            new_data = r.json()['data']['match']
            print("Parsing historical_data, Date: {}".format(new_data[0]['date']))
            if new_data[0] not in historical_data:
                historical += new_data
            elif new_data[-1] not in historical_data:
                for elem in new_data:
                    if elem not in historical_data:
                        historical.append(elem)
            inc += 1
        except:
            break

    inc = int()
    print('Start parsing fixtures...')
    while True:
        r = session.get('http://livescore-api.com/api-client/fixtures/matches.json?key={}&secret={}&page={}'
                        .format(API_KEY, API_SECRET, inc))
        print(r.json())
        data = r.json()['data']['fixtures']
        if len(data) == 0:
            break
        fixed += r.json()['data']['fixtures']
        inc += 1

    return historical, fixed


def update_json():
    actual_data = historical_data + new_historical_data
    r = HTMLSession().get('http://livescore-api.com/api-client/scores/history.json?key={}&secret={}&from={}&page=0'
                          .format(API_KEY, API_SECRET, dateparser.parse('1 year ago')))
    _id = int(r.json()['data']['match'][0]['id'])
    old_matches_c = _id - int(actual_data[0]['id'])
    if old_matches_c > 0:
        print(old_matches_c)
        actual_data = actual_data[old_matches_c:]
    with open('data/historical_data.json', 'w', encoding='utf-8') as f:
        json.dump(actual_data, f, ensure_ascii=False, indent=2, sort_keys=False)
    return actual_data


class DataProcessing(object):
    def __init__(self):
        self.result = dict()

    def run(self):
        for match in updated_historical_data:
            match['home_name'] = str(match['home_name']).lower()
            match['away_name'] = str(match['away_name']).lower()
            if match['home_name'] not in self.result.keys():
                self.result[match['home_name']] = {'wl': '', 'home': '', 'away': '', 'conceded': '', 'scored': '',
                                                   'next': {'opponent': '', 'venue': ''}, '3o': ''}
            if match['away_name'] not in self.result.keys():
                self.result[match['away_name']] = {'wl': '', 'home': '', 'away': '', 'conceded': '', 'scored': '',
                                                   'next': {'opponent': '', 'venue': ''}, '3o': ''}
            self.wl_data(match)
        for match in fixtures:
            try:
                if self.result[match['home_name']]['next']['opponent'] == '' \
                        and self.result[match['home_name']]['next']['venue'] == '':
                    self.result[match['home_name']]['next']['opponent'] = match['away_name']
                    self.result[match['home_name']]['next']['venue'] = match['location']
            except KeyError:
                pass
            try:
                if self.result[match['away_name']]['next']['opponent'] == '' \
                        and self.result[match['away_name']]['next']['venue'] == '':
                    self.result[match['away_name']]['next']['opponent'] = match['home_name']
                    self.result[match['away_name']]['next']['venue'] = match['location']
            except KeyError:
                pass
        for match in updated_historical_data:
            try:
                self.next_opponent(match)
            except:
                pass

    def wl_data(self, match):
        match['home_name'] = str(match['home_name']).lower()
        match['away_name'] = str(match['away_name']).lower()
        score = str(match['score']).split(' - ')
        if int(score[0]) > int(score[1]):
            self.result[match['home_name']]['wl'] += 'w'
            self.result[match['home_name']]['home'] += 'w'
            self.result[match['away_name']]['wl'] += 'l'
            self.result[match['away_name']]['away'] += 'l'
        elif int(score[0]) == int(score[1]):
            self.result[match['home_name']]['wl'] += 'd'
            self.result[match['home_name']]['home'] += 'd'
            self.result[match['away_name']]['wl'] += 'd'
            self.result[match['away_name']]['away'] += 'd'
        else:
            self.result[match['home_name']]['wl'] += 'l'
            self.result[match['home_name']]['home'] += 'l'
            self.result[match['away_name']]['wl'] += 'w'
            self.result[match['away_name']]['away'] += 'w'

        self.result[match['home_name']]['conceded'] += str(score[1]) + '|'
        self.result[match['home_name']]['scored'] += str(score[0]) + '|'
        self.result[match['away_name']]['conceded'] += str(score[0]) + '|'
        self.result[match['away_name']]['scored'] += str(score[1]) + '|'

    def next_opponent(self, match):
        match['home_name'] = str(match['home_name']).lower()
        match['away_name'] = str(match['away_name']).lower()
        if self.result[match['home_name']]['next']['opponent'] == match['away_name']:
            score = str(match['score']).split(' - ')
            if int(score[0]) > int(score[1]):
                self.result[match['home_name']]['3o'] += 'w'
            elif int(score[0]) == int(score[1]):
                self.result[match['home_name']]['3o'] += 'd'
            else:
                self.result[match['home_name']]['3o'] += 'l'
        elif self.result[match['away_name']]['next']['opponent'] == match['home_name']:
            score = str(match['score']).split(' - ')
            if int(score[0]) < int(score[1]):
                self.result[match['away_name']]['3o'] += 'w'
            elif int(score[0]) == int(score[1]):
                self.result[match['away_name']]['3o'] += 'd'
            else:
                self.result[match['away_name']]['3o'] += 'l'

    def create_result(self):
        for key in self.result:
            if len(self.result[key]['3o']) > 3:
                self.result[key]['3o'] = self.result[key]['3o'][len(self.result[key]['3o']) - 3:]
            elif self.result[key]['3o'] == "":
                self.result[key]['3o'] = "No Data"
            if len(self.result[key]['away']) > 5:
                self.result[key]['away'] = self.result[key]['away'][len(self.result[key]['away']) - 5:]
            elif self.result[key]['away'] == "":
                self.result[key]['away'] = "No Data"
            if len(self.result[key]['home']) > 5:
                self.result[key]['home'] = self.result[key]['home'][len(self.result[key]['home']) - 5:]
            elif self.result[key]['home'] == "":
                self.result[key]['home'] = "No Data"
            if len(self.result[key]['wl']) > 5:
                self.result[key]['wl'] = self.result[key]['wl'][len(self.result[key]['wl']) - 5:]
            elif self.result[key]['wl'] == "":
                self.result[key]['wl'] = "No Data"
            if len(str(self.result[key]['conceded'][:-1]).split('|')) > 5:
                conceded = str(self.result[key]['conceded'][:-1]).split('|')
                conceded.reverse()
                conceded = conceded[:5]
            else:
                conceded = str(self.result[key]['conceded'][:-1]).split('|')
            self.result[key]['conceded'] = float('{:.2f}'.format(sum(int(i) for i in conceded) / len(conceded)))
            if self.result[key]['conceded'] == 0.0:
                self.result[key]['conceded'] = "No Data"
            if len(str(self.result[key]['scored'][:-1]).split('|')) > 5:
                scored = str(self.result[key]['scored'][:-1]).split('|')
                scored.reverse()
                scored = scored[:5]
            else:
                scored = str(self.result[key]['scored'][:-1]).split('|')
            self.result[key]['scored'] = float('{:.2f}'.format(sum(int(i) for i in scored) / len(scored)))
            if self.result[key]['scored'] == 0.0:
                self.result[key]['scored'] = "No Data"
            if self.result[key]['next']['opponent'] == "":
                self.result[key]['next']['opponent'] = "No Data"
            if self.result[key]['next']['venue'] == "":
                self.result[key]['next']['venue'] = "No Data"
        with open('data/statistic.json', 'w', encoding='utf-8') as f:
            json.dump(self.result, f, ensure_ascii=False, indent=2, sort_keys=True)
        return self.result


@bot.message_handler(content_types=['text'])
def message_handler(message):
    if message.text == '/start':
        return
    message_text = message.text.lower()
    soccer_teams = stat.keys()
    if message_text in soccer_teams:
        bot.send_message(message.from_user.id, send_result(message_text))
        return
    for team in soccer_teams:
        if difflib.SequenceMatcher(a=team, b=message_text).ratio() > 0.95:
            bot.send_message(message.from_user.id, send_result(team))
            return
    bot.send_message(message.from_user.id, 'Team "{}" not found :('.format(message_text))
    return


def send_result(team_name):
    return "Team name: {}" \
           "Last results at home = {}\n" \
           "Last results away = {}\n" \
           "Last win/lose results = {}\n" \
           "Average goals conceded = {}\n" \
           "Average goals scored = {}\n" \
           "Next match:\n\t" \
           "Opponent = {}\n\t" \
           "Venue = {}\n\t" \
           "Last matches vs that opponent = {}\n" \
           "====================================="\
           .format(team_name,
                   stat[team_name]['home'],
                   stat[team_name]['away'],
                   stat[team_name]['wl'],
                   stat[team_name]['conceded'],
                   stat[team_name]['scored'],
                   stat[team_name]['next']['opponent'],
                   stat[team_name]['next']['venue'],
                   stat[team_name]['3o'])


if __name__ == '__main__':
    check_point = False
    while True:
        historical_data, _from = create_start_point()
        new_historical_data, fixtures = read_data()
        updated_historical_data = update_json()
        print('Run DataProcessing...')
        obj = DataProcessing()
        obj.run()
        print('Creating Results...')
        stat = obj.create_result()
        print('Start bot polling')
        if check_point is False:
            bot.polling(none_stop=True, interval=0)
            check_point = True
        print('Started')
        del updated_historical_data,\
            new_historical_data, \
            historical_data,\
            fixtures, \
            _from

        time.sleep(86400)



