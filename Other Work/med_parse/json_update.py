import json
import re


def check_keys():
    broken = {1: [], 2: [], 3: [], 4: [], 5: []}
    for keys, drugs_list in data.items():
        for key, value in drugs_list.items():
            if key not in value.split('therapeutic action')[0]:
                broken[int(keys)].append(key)
    for i in broken.values():
        print(i)


def split_row(key: str, group: int, text: str):
    text = re.sub(r'\u2013\\n|\u2013|\\n\u2022|prescription under medical supervision', '', text)

    text = text.split('therapeutic action')[1] if 'therapeutic action' in text else text

    spl = re.findall(r'([^"]*)\nindications\n([^"]*)\npresentation\n([^"]*)\ndosage\n([^"]*)\nduration\n([^"]*)'
                     r'\ncontra-indications, adverse effects, precautions\n([^"]*)\nremarks\n([^"]*)', text)
    if len(spl) == 0:
        spl = re.findall(r'([^"]*)\nindications\n([^"]*)\npresentation\n([^"]*)\ndosage and duration\n([^"]*)'
                         r'\ncontra-indications, adverse effects, precautions\n([^"]*)\nremarks\n([^"]*)', text)
        if len(spl) == 0:
            return 0

        spl = spl[0]
        return {"group": group, "key": key, 'therapeutic action': spl[0],
                'indications': spl[1], 'presentation': spl[2], 'dosage and duration': spl[3],
                'contra-indications, adverse effects, precautions': spl[4], 'remarks': spl[5]}
    spl = spl[0]
    return {"group": group, "key": key, 'therapeutic action': spl[0],
            'indications': spl[1], 'presentation': spl[2], 'dosage and duration': spl[3] + '\n' + spl[4],
            'contra-indications, adverse effects, precautions': spl[5], 'remarks': spl[6]}


def update():
    dis = []
    not_dis = []
    for group, values in data.items():
        for key, text in values.items():
            res = split_row(key, group, text)
            if res == 0:
                not_dis.append([key, group, text])
            else:
                dis.append(res)
    print(len(dis))
    print(len(not_dis))


if __name__ == '__main__':
    data = json.load(open('res.json', 'r', encoding='utf-8'))
    # check_keys()
    update()

