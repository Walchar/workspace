import json

res = {1: {}, 2: {}, 3: {}, 4: {}, 5: {}}

not_recognized = [[] for _ in range(5)]


def get_optimal_key(text: str, req_keys: list, elem: int):
    if len(req_keys) != 0:
        count, req_key = 2**40, None
        for key in req_keys:
            dist = len(text.split(key)[0])
            if dist < count:
                count, req_key = dist, key
        res[elem + 1][req_key] = text
        return req_key
    else:
        not_recognized[elem].append(text)
        return 0


def create_res(elem: int):
    selected_keys = keys[elem].lower().split('\n')
    selected_data = data[elem].lower().split('– storage:')
    print(len(selected_keys))
    print(len(selected_data))

    for text in selected_data:
        req_keys = []
        for key in selected_keys:
            req_keys.append(key) if key in text else None
        key = get_optimal_key(text, req_keys, elem)
        None if key is 0 else selected_keys.remove(key)

    print(not_recognized[elem])
    print(len(not_recognized[elem]))
    print('===')


if __name__ == '__main__':
    data = str(open('data/text.txt', 'r', encoding='utf-8').read()).lower().split('=\=\=')
    keys = str(open('data/keys.txt', 'r', encoding='utf-8').read()).lower().split('\n====\n')

    for i in range(len(data)):
        create_res(i)

    json.dump(not_recognized, open('not_recognized.json', 'w', encoding='utf-8'))
    json.dump(res, open('res.json', 'w', encoding='utf-8'))
