from flask import Flask, request
from werkzeug.utils import secure_filename
from gevent import Timeout
import os
import subprocess
import json
import re
import threading

current_folder = os.getcwd()
app = Flask(__name__)
in_exec = list()


def read_json():
    with open('users.json', 'r', encoding='utf-8') as f:
        return json.load(f)


def save_results(path, result):
    with open('results.json', 'r', encoding='utf-8') as f:
        results = json.load(f)
        results[path] = result
        json.dump(results, open('results.json', 'w', encoding='utf-8'))
    return 0


def read_file(path):
    # Add current file to the in_execution list
    in_exec.append(path)
    # Set running timer
    timeout = Timeout(200)
    if '.py' not in path:
        return save_results(path, "This file format is not supported for execution")
    try:
        # Start timer
        timeout.start()
        res = str()
        # File execution
        command = "python3 {}".format(current_folder + '/users/' + path)
        process = subprocess.Popen([command], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        # Get results
        out = process.stdout.readlines()
        for elem in out:
            res += elem.decode('utf-8').replace('\n', '|#|')
        error = process.stderr.readlines()
        res += '|#||#|'
        for elem in error:
            res += elem.decode('utf-8').replace('/home/walchar/Work_folder/cloud_system/', '').replace('\n', '|#|')
        # Check for ModuleNotFoundError
        if 'ModuleNotFoundError' in res:
            # Get lib name
            lib = re.findall(r"ModuleNotFoundError: No module named '(\w*)'", res)
            try:
                # Trying to install this lib from pip repository
                subprocess.check_call('/usr/bin/python3 -m pip install {}'.format(lib[0]), shell=True)
                # Run execution again with required libs
                return read_file(path)
            except:
                res += "|#| We can't find '{}' lib in pip".format(lib[0])
                # Remove current file from the in_execution list
                in_exec.remove(path)
                # Start saving execution results
                return save_results(path, res)
        # Remove current file from the in_execution list
        in_exec.remove(path)
        # Start saving execution results
        return save_results(path, res)
    except:
        return "Your program works to long"


def allowed_file(filename):
    ALLOWED_EXTENSIONS = ['txt', 'py', 'json', 'csv']
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@app.route("/remove/<username>/<file>")
def remove(username, file):
    os.system(f'rm -rf {current_folder}/users/{username}/{file}')
    try:
        with open('results.json', 'r', encoding='utf-8') as f:
            results = json.load(f)
        del results[username + '/' + file]
        json.dump(results, open('results.json', 'w', encoding='utf-8'))
    except:
        pass
    return "file successfully removed, update window"


@app.route("/execute/<username>/<file>")
def execute(username, file):
    if '.py' not in file:
        return "This file format is not supported for execution"
    with open('results.json', 'r', encoding='utf-8') as f:
        results = json.load(f)
        try:
            res = results[username + '/' + file]
            if username == 'Gst':
                os.system(f'rm {current_folder}/users/{username}/{file}')
                with open('results.json', 'r', encoding='utf-8') as nf:
                    results = json.load(nf)
                del results[username + '/' + file]
                json.dump(results, open('results.json', 'w', encoding='utf-8'))
            return res
        except:
            if username + '/' + file in in_exec:
                return "This file is in execution process, the maximum execution time is 200 seconds"
            else:
                return "Only uploaded files can be executed"


@app.route('/register/<username>/<password>')
def registration(username, password):
    users[username] = password
    json.dump(users, open('users.json', 'w', encoding='utf-8'))
    os.system('mkdir {}/users/{}'.format(current_folder, username))
    return 'User Created'


@app.route('/login/<username>/<password>')
def login(username, password):
    if username in users.keys():
        if users[username] == password:
            return 'Success'
        else:
            return 'incorrect password'
    else:
        return 'This user is not registered'


@app.route('/check_files/<username>')
def check_files(username):
    res = str()
    files = os.listdir('{}/users/{}'.format(current_folder, username))
    for elem in files:
        res += 'users/{}/{},'.format(username, elem)
    return res.strip(',')


@app.route('/file_upload/<username>', methods=['GET', 'POST'])
def file_upload(username):
    # Checking request type
    if request.method == 'POST':
        file = request.files['files']
        # Checking file format
        if file and allowed_file(file.filename):
            # Checking is filename valid
            filename = secure_filename(file.filename)
            files = os.listdir(f"{current_folder}/users/{username}")
            # changing filename by adding number at the end of the filename  if user already add file with this filename
            while filename in files:
                if filename.split('.')[0][-2] == '_':
                    num = re.findall(r'_(\d\d?)\.', filename)
                    filename = re.split(r'_\d\d?\.', filename)
                    filename = f"{filename[0]}_{str(int(num[0]) + 1)}.{filename[1]}"
                else:
                    filename = filename.split('.')
                    filename = f"{filename[0]}_1.{filename[1]}"
            file.save(f"{current_folder}/users/{username}/{filename}")

            # if file received form an guest user
            if username == 'Gst':
                if '.py' not in filename:
                    return "Only .py files allowed to execute"
                # Starting file execution for guest user in another thread
                threading.Thread(target=read_file, args=(f"{username}/{filename}",)).start()
                return f'Successful {filename}'
            # Starting file execution for authorized user in another thread
            threading.Thread(target=read_file, args=(f"{username}/{filename}",)).start()
            return 'Successfully uploaded'
        elif username == 'Gst':
            return "Only .py files allowed to execute"
        else:
            return "Only 'txt', 'py', 'json', 'csv' files allowed to upload"


if __name__ == '__main__':
    users = read_json()
    app.debug = True
    app.run('192.168.1.20', 25555, ssl_context=('cert.pem', 'key.pem'))
