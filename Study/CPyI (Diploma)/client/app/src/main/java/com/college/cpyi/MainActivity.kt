package com.college.cpyi

import kotlinx.android.synthetic.main.activity_main.*
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.beust.klaxon.Parser
import kotlinx.android.synthetic.main.list_item_view.view.*
import okhttp3.*
import java.io.File
import okhttp3.OkHttpClient
import java.io.FileNotFoundException
import java.net.ConnectException
import java.security.KeyStore
import java.security.cert.CertificateFactory
import javax.net.ssl.*


class MainActivity : AppCompatActivity() {

    companion object {
        var login: String = ""
        var pass: String = ""
        var client = OkHttpClient()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val loginView = LoginActivity
        if (savedInstanceState == null) {
            startActivityForResult(loginView.newIntent(this), 2)
        }
        setContentView(R.layout.activity_main)
        recyclerview.layoutManager = LinearLayoutManager(this)
        recyclerview.itemAnimator = DefaultItemAnimator()
        recyclerview.setHasFixedSize(true)

        val cert = resources.openRawResource(R.raw.cert)
        val ca = CertificateFactory.getInstance("X.509").generateCertificate(cert)
        cert.close()
        val keyStore = KeyStore.getInstance(KeyStore.getDefaultType())
        keyStore.load(null, null)
        keyStore.setCertificateEntry("ca", ca)
        val tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm()
        val tmf = TrustManagerFactory.getInstance(tmfAlgorithm)
        tmf.init(keyStore)
        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, tmf.trustManagers, null)
        client = OkHttpClient.Builder()
            .sslSocketFactory(sslContext.socketFactory, tmf.trustManagers[0] as X509TrustManager)
            .hostnameVerifier({hostname, session -> true})
            .build()


        val permission = android.Manifest.permission.READ_EXTERNAL_STORAGE
        ActivityCompat.requestPermissions(this, listOf(permission).toTypedArray(), 123)


        executeButton.setOnClickListener {
            val intent = Intent().setType("*/*").setAction(Intent.ACTION_GET_CONTENT)
            startActivityForResult(Intent.createChooser(intent, "Select a file"), 100)
        }
        uploadButton.setOnClickListener {
            val intent = Intent().setType("*/*").setAction(Intent.ACTION_GET_CONTENT)
            startActivityForResult(Intent.createChooser(intent, "Select a file"), 200)
        }
        checkButton.setOnClickListener {
            
            val res = Get(String.format("https://5.58.208.122:25555/check_files/%s", login), client).execute().get()
            if (res == "ConnectException: Internet connection error"){
                this.toast("ConnectException: Internet connection error")
            }
            val filesList = res.split(",").toList()
            recyclerview.adapter = Adapter(filesList.sorted(), responseText, client)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            val selected = data?.data
            val path = selected?.path.toString().replace("/document/primary:", "/storage/emulated/0/")
            val resp = Post("https://5.58.208.122:25555/file_upload/Gst", File(path), client).execute().get()
            if (resp == "ConnectException: Internet connection error"){
                this.toast("ConnectException: Internet connection error")
            }
            val spResp = resp.split(' ')
            if (spResp[0] == "Successful") {
                while (true) {
                    Thread.sleep(2_000)
                    var res = Get(String.format("https://5.58.208.122:25555/execute/Gst/%s", spResp[1]), client)
                        .execute().get()
                    if (res == "ConnectException: Internet connection error"){
                        this.toast("ConnectException: Internet connection error")
                    }
                    if (res != "This file is in execution process, the maximum execution time is 200 seconds") {
                        res = res.replace(oldValue = "|#|", newValue = "\n")
                        responseText.text = res
                        break
                    }
                }
            } else {
                responseText.text = resp
            }
        }
        if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            val selected = data?.data
            val path = selected?.path.toString().replace("/document/primary:", "/storage/emulated/0/")
            var res = Post(String.format("https://5.58.208.122:25555/file_upload/%s", login),
                File(path), client).execute().get()
            if (res == "ConnectException: Internet connection error"){
                this.toast("ConnectException: Internet connection error")
            }
            res = res.replace(oldValue = "|#|", newValue = "\n")
            responseText.text = res
    }
        if (requestCode == 2) {
            if (data?.extras?.get("visible") == "VIS") {
                executeButton.visibility = View.GONE
                loginArea.visibility = View.VISIBLE
            }
        }
    }

    class Adapter(private val values: List<String>, val response_t: TextView, val client: OkHttpClient) :
        RecyclerView.Adapter<Adapter.ViewHolder>() {

        override fun getItemCount() = values.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_item_view, parent, false)
            return ViewHolder(itemView, response_t, client)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textView.text = values[position]
    }

        class ViewHolder(itemView: View, response_t: TextView, client: OkHttpClient) :
            RecyclerView.ViewHolder(itemView) {
            var textView: TextView

            init {

                textView = itemView.list_itemText
                itemView.setOnClickListener {
                    val file = textView.text.split('/')[2]
                    val builder = AlertDialog.Builder(itemView.context)
                    builder.setMessage(R.string.exec_confirm)
                        .setNeutralButton("Remove") { dialog, id ->
                            val res = Get(String.format("https://5.58.208.122:25555/remove/%s/%s", login, file), client)
                            .execute().get()
                            response_t.text = res
                        }
                        .setNegativeButton(R.string.req_neg) { dialog, id -> }
                        .setPositiveButton(R.string.req_pos) { dialog, id ->
                            var res = Get(
                                String.format("https://5.58.208.122:25555/execute/%s/%s", login, file), client)
                                .execute().get()
                            res = res.replace(oldValue = "|#|", newValue = "\n")
                            response_t.text = res
                        }
                    val alert = builder.create()
                    alert.show()
                }
            }
        }
    }
}





class Get(val url: String, val client: OkHttpClient) : AsyncTask<Void, Void, String>() {

    override fun doInBackground(vararg params: Void?): String? {
        try {
            val request = Request.Builder()
                .url(String.format(url))
                .build()
            val response = client.newCall(request)
            return response.execute().body()?.string()
        } catch (e: ConnectException) {
            return "ConnectException: Internet connection error"
        }
    }
}

class Post(val url: String, val file: File, val client: OkHttpClient) : AsyncTask<Void, Void, String>() {

    override fun doInBackground(vararg params: Void?): String? {
        try {
            val requestBody = MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("files", file.name, RequestBody.create(MediaType.parse("text/*"), file))
                .build()
            val request = Request.Builder().url(url).post(requestBody).build()
            val response = client.newCall(request)
            return response.execute().body()?.string()
        } catch (e: ConnectException) {
            return "ConnectException: Internet connection error"
        } catch (e: FileNotFoundException){
            return "FileNotFoundException: Use normal file explorer"
        }

    }
}

fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}