package com.college.cpyi
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.*
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.login.*

class LoginActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)
        loginButton.setOnClickListener {
            val login = loginEdit.text.toString()
            val pass = passEdit.text.toString()
            MainActivity.login = login
            MainActivity.pass = pass
            var correct_data = 0

            if (login.length >= 4 && pass.length >= 4){ correct_data = 1}
            else {
                val builder = AlertDialog.Builder(this)
                builder.setMessage(R.string.incorrect_data)
                    .setPositiveButton(R.string.ok_but) { dialog, id -> }
                val alert = builder.create()
                alert.show()
            }
            if (correct_data == 1) {
                val res = Get(String.format("https://5.58.208.122:25555/login/%s/%s", login, pass), MainActivity.client).execute().get()
                if (res == "ConnectException: Internet connection error"){
                    this.toast("ConnectException: Internet connection error")
                }
                if (res == "Success"){
                    val intent = Intent()
                    intent.putExtra("visible", "VIS")
                    setResult(2, intent)
                    finish()
                }
                else if (res == "This user is not registered") {
                    val builder = AlertDialog.Builder(this)
                    builder.setMessage(R.string.req_message)
                        .setTitle(R.string.req_title)
                        .setNegativeButton(R.string.req_neg) { dialog, id -> }
                    builder.setPositiveButton(R.string.req_pos) { dialog, id ->
                        Get(String.format("https://5.58.208.122:25555/register/%s/%s", login, pass), MainActivity.client).execute().get()
                        val intent = Intent()
                        intent.putExtra("visible", "VIS")
                        setResult(2, intent)
                        finish()
                    }
                    val alert = builder.create()
                    alert.show()
                }
                else if (res == "incorrect password") {
                    val builder = AlertDialog.Builder(this)
                    builder.setMessage(R.string.incorrect_pass)
                        .setPositiveButton(R.string.ok_but) { dialog, id -> }
                    val alert = builder.create()
                    alert.show()
                }
            }
        }
        guestButton.setOnClickListener {
        finish()
        }
    }


    override fun onBackPressed() {
        toast("You entered guest mode")
        super.onBackPressed()
    }

    companion object{
        fun newIntent(context: Context) : Intent{
            return Intent(context, LoginActivity::class.java)
        }
    }

    fun Context.toast(message: CharSequence){
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}




