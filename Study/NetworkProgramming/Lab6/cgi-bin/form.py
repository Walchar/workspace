#!/usr/bin/env python3
import cgi
import html

form = cgi.FieldStorage()
text1 = form.getfirst("TEXT_1", "не задано")
text2 = form.getfirst("TEXT_2", "не задано")
text1 = html.escape(text1)
text2 = html.escape(text2)

print("Content-type: text/html\n")
print("""<!DOCTYPE HTML>
        <html>
        <head>
            <meta charset="utf-8">
            <title>Вітаємо</title>
        </head>
        <body>""")

print("<h1>Вітаємо</h1>")
print("<p>Ім'я: {}</p>".format(text1))
print("<p>Прізвище: {}</p>".format(text2))

print("""</body>
        </html>""")