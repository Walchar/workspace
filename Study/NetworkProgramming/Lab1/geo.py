from geopy.geocoders import Nominatim
from geopy.exc import GeocoderTimedOut
from urllib import request, parse
import json

address = 'Ternopil Oblast, Petrykiv, Svitankova 89, 47720'


def geo_1():
    geolocator = Nominatim(user_agent="Lab 1 Ukraine TNTU")
    try:
        location = geolocator.geocode(address)
        print(location.latitude, location.longitude)
    except GeocoderTimedOut as e:
        print("Error: geocode failed on input %s with message %s" % (address, e.__doc__))


def geo_2():
    geolocator = Nominatim(user_agent="Lab 1 Ukraine TNTU")

    location = geolocator.geocode(address, addressdetails=True)
    print(location.raw['address'])
    for key in location.raw['address']:
        print(key)
    print(location.raw['address'][key])


def geo_3():

    query = "q=%s&format=json" % (parse.quote(address),)
    response = request.urlopen("http://nominatim.openstreetmap.org/search.php?" + query)
    content = response.read()
    print(content)
    test = json.loads(content)
    print(test)
    for el in test:
        print(el['display_name'] + ':', el['lat'], el['lon'])


if __name__ == '__main__':
    geo_1()
    print('=====')
    geo_2()
    print('=====')
    geo_3()
