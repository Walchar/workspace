import numpy as np
import h5py


class DNN(object):
    def __init__(self, layers_dims, num_iterations=3000, verbose=True, learning_rate=0.0075):
        self.X = None
        self.Y = None
        self.m = None
        self.layers = layers_dims
        self.num_iter = num_iterations
        self.verbose = verbose
        self.learning_rate = learning_rate
        self.params = None

    def compute_cost(self, A_last, Y):
        cost = - (1 / self.m) * np.sum(np.multiply(Y, np.log(A_last)) + np.multiply(1 - Y, np.log(1 - A_last)))
        return cost

    def forward_propagation_step(self, A_prev, W, b, activation):
        Z = W @ A_prev + b
        A = 1 / (1 + np.exp(-Z)) if activation == "sigmoid" else np.maximum(0, Z)
        cache = (W, b, A_prev, Z)
        return A, cache

    def forward_propagation_whole_process(self, X, parameters):
        caches = {}
        A = X
        L = len(parameters['W'])
        for l in range(1, L):
            A_prev = A
            A, cache = self.forward_propagation_step(A_prev, parameters['W'][l], parameters['b'][l], 'relu')
            caches[l] = cache
        A_last, cache = self.forward_propagation_step(A, parameters['W'][L], parameters['b'][L], 'sigmoid')
        caches[L] = cache
        return A_last, caches

    def backward_propagation_step(self, dL_dA, cache, activation):
        W, b, A_prev, Z = cache
        dg_dz = None
        if activation == "relu":
            dg_dz = Z
            dg_dz[dg_dz <= 0] = 0
            dg_dz[dg_dz > 0] = 1
        elif activation == "sigmoid":
            sig = 1 / (1 + np.exp(-Z))
            dg_dz = sig * (1 - sig)
        dL_dZ = dL_dA * dg_dz
        dL_dW = (1 / self.m) * dL_dZ @ A_prev.T
        dL_db = (1 / self.m) * np.sum(dL_dZ, axis=1, keepdims=True)
        dL_dA_prev = W.T @ dL_dZ
        return dL_dA_prev, dL_dW, dL_db

    def backward_propagation_whole_process(self, A_last, Y, caches):
        dL_dA = {}
        dL_dW = {}
        dL_db = {}
        L = len(caches)
        Y = Y.reshape(A_last.shape)
        dL_dA[L] = -(Y / A_last) + ((1 - Y) / (1 - A_last))
        current_cache = caches[L]
        dL_dA[L - 1], dL_dW[L], dL_db[L] = self.backward_propagation_step(dL_dA[L], current_cache, 'sigmoid')
        for l in reversed(range(1, L)):
            current_cache = caches[l]
            dL_dA[l - 1], dL_dW[l], dL_db[l] = self.backward_propagation_step(dL_dA[l], current_cache, 'relu')
        grads = {'W': dL_dW, 'b': dL_db}
        return grads

    def initialize_parameters(self):
        params = {'W': {}, 'b': {}}
        for l in range(1, len(self.layers)):
            params['W'][l] = np.random.randn(self.layers[l], self.layers[l - 1]) / np.sqrt(self.layers[l - 1])
            params['b'][l] = np.zeros((self.layers[l], 1))
        return params

    def fit(self, X_train, Y_train):
        self.X = X_train
        self.Y = Y_train
        self.m = self.X.shape[1]

        costs = []  # to track of cost
        iters = []
        params = self.initialize_parameters()
        for i in range(self.num_iter):
            A_last, caches = self.forward_propagation_whole_process(self.X, params)
            cost = self.compute_cost(A_last, self.Y)
            grads = self.backward_propagation_whole_process(A_last, self.Y, caches)
            params = self.update_parameters(params, grads, self.learning_rate)

            if self.verbose and i % 100 == 0:
                print("Cost after iteration {}: {}".format(i, cost))
            if i % 100 == 0:
                costs.append(cost)
                iters.append(i)
        self.params = params

    def predict(self, X):
        A_last, _ = self.forward_propagation_whole_process(X, self.params)
        Y_pred = (A_last >= 0.5).astype(int)
        return Y_pred

    @staticmethod
    def update_parameters(parameters, grads, learning_rate):
        for param in grads['W']:
            parameters['W'][param] -= learning_rate * grads['W'][param]
            parameters['b'][param] -= learning_rate * grads['b'][param]
        return parameters

    def save_model(self):
        np.save('model', [self.params])
        print('model saved successfully')

    def load_model(self):
        self.params = np.load('model.npy')[0]
        print('model loaded successfully')


def load_dataset():
    train_dataset = h5py.File('data/train_catvnoncat.h5', "r")
    X_train = np.array(train_dataset["train_set_x"][:])
    Y_train = np.array(train_dataset["train_set_y"][:])

    test_dataset = h5py.File('data/test_catvnoncat.h5', "r")
    X_test = np.array(test_dataset["test_set_x"][:])
    Y_test = np.array(test_dataset["test_set_y"][:])

    classes = ['non-cat', 'cat']

    return X_train, Y_train, X_test, Y_test, classes


def data_preprocess():
    X_train, Y_train, X_test, Y_test, classes = load_dataset()

    img_size = X_train[0].shape

    X_train_scaled = X_train.reshape(X_train.shape[0], img_size[0] * img_size[1] * img_size[2]).T / 255.
    X_test_scaled = X_test.reshape(X_test.shape[0], img_size[0] * img_size[1] * img_size[2]).T / 255.

    Y_train = Y_train.reshape(Y_train.shape[0], -1).T
    Y_test = Y_test.reshape(Y_test.shape[0], -1).T
    return X_train_scaled, X_test_scaled, Y_train, Y_test


if __name__ == '__main__':
    X_train_scaled, X_test_scaled, Y_train, Y_test = data_preprocess()
    layers_dims = [X_train_scaled.shape[0], 20, 7, 5, 1]

    model = DNN(layers_dims, num_iterations=3000, verbose=True, learning_rate=0.0075)

    model.load_model()
    print('Train accuracy = {:.3%}'.format(np.mean(model.predict(X_train_scaled) == Y_train)))
    print('Test accuracy = {:.3%}'.format(np.mean(model.predict(X_test_scaled) == Y_test)))
