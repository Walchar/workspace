import pandas as pd
from nltk.tokenize import word_tokenize
from nltk.stem.porter import PorterStemmer
from gensim import corpora
import gensim
from nltk.corpus import stopwords
import re
from nltk.probability import FreqDist


class TopicModeling(object):
    def __init__(self, data):
        self.data_set = data
        self.data_vectorized()

    def data_transform(self):
        stop_words = set(stopwords.words('english'))
        stemmer = PorterStemmer()
        texts = []
        all_tokens = []
        for elem in self.data_set:
            text = re.sub(r'[^\w\s]', '', elem)
            tokens = word_tokenize(text)
            tokens = [token for token in tokens if token not in stop_words]
            tokens = [stemmer.stem(token) for token in tokens]
            all_tokens += tokens
            texts.append(tokens)
        most_common = FreqDist(all_tokens).most_common()[:20]
        for text in texts:
            for token in most_common:
                for i in range(text.count(token[0])):
                    text.remove(token[0])
        return texts

    def data_vectorized(self):
        texts = self.data_transform()
        dictionary = corpora.Dictionary(texts)
        corpus = [dictionary.doc2bow(text) for text in texts]
        ldamodel = gensim.models.ldamodel.LdaModel(corpus, num_topics=24, id2word=dictionary, passes=20, random_state=0)
        [print(topic) for topic in ldamodel.print_topics(num_words=5)]


if __name__ == '__main__':
    data_set = list(pd.read_csv('./data/voted-kaggle-dataset.csv').dropna(subset=['Description'])['Description'])
    TopicModeling(data_set)

