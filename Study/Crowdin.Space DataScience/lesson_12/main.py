import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression


class TitanicClassifier(object):
    def __init__(self, x_train, y_train, x_test):
        self.x_train = x_train
        self.y_train = y_train
        self.x_test = x_test

    def random_forest(self):
        clf = RandomForestClassifier(n_estimators=10, criterion='entropy').fit(self.x_train, self.y_train)
        predicrion = clf.predict(self.x_test)
        print('Random forest regression accuracy train: {}'.format(clf.score(self.x_train, self.y_train)))
        return predicrion

    def svc(self):
        clf = SVC(degree=3, kernel='poly', gamma='scale').fit(self.x_train, self.y_train)
        predicrion = clf.predict(self.x_test)
        print('SVC accuracy train: {}'.format(clf.score(self.x_train, self.y_train)))
        return predicrion

    def regression(self):
        clf = LogisticRegression().fit(self.x_train, self.y_train)
        predicrion = clf.predict(self.x_test)
        print('Logistic Regression accuracy train: {}'.format(clf.score(self.x_train, self.y_train)))
        return predicrion


def load_data():
    x_train = pd.read_csv('data/train.csv')
    y_train = x_train['Survived']
    x_train = x_train.drop(['Survived'], axis=1)
    x_test = pd.read_csv('data/test.csv')

    for index in x_train.index:
        x_train.loc[index, 'Name'] = str(x_train.loc[index, 'Name']).split(',')[0]
    for index in x_test.index:
        x_test.loc[index, 'Name'] = str(x_test.loc[index, 'Name']).split(',')[0]

    from sklearn.preprocessing import LabelEncoder
    to_encode = ['Name', 'Sex', 'Ticket', 'Cabin', 'Embarked']
    encoder = LabelEncoder()

    for item in to_encode:
        encoder.fit(x_train[item].fillna(''))
        x_train[item] = encoder.transform(x_train[item].fillna(''))
        encoder.fit(x_test[item].fillna(''))
        x_test[item] = encoder.transform(x_test[item].fillna(''))

    x_train = x_train.fillna(0)
    x_test = x_test.fillna(0)

    return x_train.values, y_train.values, x_test.values


if __name__ == '__main__':
    classifier = TitanicClassifier(*load_data())
    res1 = classifier.regression()
    res2 = classifier.random_forest()
    res3 = classifier.svc()

    votes = [1 if res1[i] + res2[i] + res3[i] > 1 else 0 for i in range(len(res1))]

    data_set = pd.read_csv('data/test.csv', usecols=['PassengerId'])
    data_set['Survived'] = votes
    data_set.to_csv('Prediction.csv', index=False)


