import re
import pandas as pd

full_d = {'january': '01', 'february': '02', 'march': '03', 'april': '04', 'may': '05', 'june': '06', 'july': '07',
          'august': '08', 'september': '09', 'october': '10', 'november': '11', 'december': '12'}


class DatePreprocessing(object):
    def __init__(self):
        self.df = pd.DataFrame(columns=['res', 'index'])
        self.num = 0
        self.row, self.date = None, None
        self.read_file()
        self.df.sort_values('res').to_csv('result.csv', index=False)

    def read_file(self):
        with open('dates.txt', 'r', encoding='utf-8') as f:
            for row in f:
                row = re.sub(r'\d{3}-\d{3}-\d{4}', '', row.lower().replace(', ', ' ').replace('. ', ' '))
                for elem in full_d:
                    if len(re.findall(elem + r' \d\d? \d{4}', row)) != 0 or len(re.findall(elem[:3] + r' \d\d? \d{4}', row)) != 0:
                        reg = re.findall(r' (\d\d?) (\d{4})', row)
                        row = '{} {} {}'.format(reg[0][0], full_d[elem], reg[0][1])
                        break
                    row = row.replace(elem, full_d[elem]) if elem in row else row.replace(elem[:3], full_d[elem])
                self.row = row
                self.find_date()

    def find_date(self):
        pattern = re.compile(r'(?:\d\d?[-\\/]\d\d?[-\\/](?:\d{4}|\d\d)\b)|(?:\d{2,4}[-\\/]\d\d?[-\\/]\d\d?)'
                             r'|(?:\d\d? \d\d? (?:\d{4}|\d\d)\b)|(?:\d{2,4} \d\d? \d\d?)|'
                             r'(?:\b\d{4}[- \\/]\d\d?\b)|(?:\b\d\d?[- \\/]\d{4}\b)|(?:\b\d{4}\b)|(?:[a-zA-Z]\d{4}\b)')
        dates = re.findall(pattern, self.row)
        for date in dates:
            date = re.sub('[a-zA-Z]', '', date)
            self.date = date
            self.processing()
            self.num += 1

    def processing(self):
        s = 0
        self.date = str(self.date).strip(' ')
        date = str(re.sub(r'[ \\/]', '-', self.date)).split('-')
        for i in range(len(date)):
            date[i] = '0' + str(date[i]) if len(date[i]) == 1 else date[i]
        job = [0, "{'res': date[0] + '-01-01', 'index': self.num}",
               "{'res': date[1] + '-' + date[0] + '-01', 'index': self.num}",
               "{'res': date[1] + '-01-01', 'index': self.num}",
               "{'res': date[2] + '-' + date[1] + '-' + date[0], 'index': self.num}",
               "{'res': date[2] + '-' + date[0] + '-' + date[1], 'index': self.num}",
               "{'res': '19' + date[2] + '-' + date[0] + '-' + date[1], 'index': self.num}",
               "{'res': '19' + date[2] + '-' + date[1] + '-' + date[0], 'index': self.num}"]
        s = 1 if s == 0 and len(date) == 1 and len(date[0]) == 4 else s
        s = 2 if s == 0 and len(date) == 2 and int(date[0]) < 13 and len(date[1]) == 4 else s
        s = 3 if s == 0 and len(date) == 2 and int(date[0]) > 13 and len(date[1]) == 4 else s
        s = 4 if s == 0 and len(date) == 3 and len(date[2]) == 4 and 32 > int(date[0]) > 12 else s
        s = 5 if s == 0 and len(date) == 3 and len(date[2]) == 4 and 32 > int(date[1]) > 12 else s
        s = 6 if s == 0 and len(date) == 3 and len(date[2]) == 2 and int(date[2]) > 31 and 32 > int(date[1]) > 12 else s
        s = 5 if s == 0 and len(date) == 3 and len(date[2]) == 4 and '/' in self.date or '\\' in self.date else s
        s = 4 if s == 0 and len(date) == 3 and len(date[2]) == 4 and ' ' in self.date else s
        s = 6 if s == 0 and len(date) == 3 and len(date[2]) == 2 and '/' in self.date or '\\' in self.date else s
        s = 7 if s == 0 and len(date) == 3 and len(date[2]) == 2 and ' ' in self.date else s
        self.df = self.df.append(eval(job[s]), ignore_index=True) if s != 0 else self.df


if __name__ == '__main__':
    DatePreprocessing()
