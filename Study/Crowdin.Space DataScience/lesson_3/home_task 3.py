from scipy.spatial import distance
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
import collections


result = {'correct_results': 0, 'all_results': 0}
best_accuracy = {'best_res': 0, 'K': 0}


class KNN(object):
    def __init__(self, k):
        result['correct_results'], result['all_results'] = 0, 0
        self.k = k
        self.X_train, self.X_test = None, None
        self.y_train, self.y_test = None, None
        self.split_data()

    def split_data(self):
        iris = load_iris()
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(iris.data, iris.target, test_size=0.3)

    def calculate(self):
        for i in range(len(self.X_test)):
            all_ec_dist = dict()
            for j in range(len(self.X_train)):
                dist = distance.euclidean(self.X_test[i], self.X_train[j])
                all_ec_dist[dist] = self.y_train[j]
            all_distances = collections.OrderedDict(sorted(all_ec_dist.items()))
            predicted_y = self.found_most_major(list(all_distances.values())[:self.k])
            if predicted_y == self.y_test[i]:
                result['correct_results'] += 1
            result['all_results'] += 1
        self.check_accuracy()

    def check_accuracy(self):
        accuracy = int(100 / result['all_results'] * result['correct_results'])
        if accuracy > best_accuracy['best_res']:
            best_accuracy['best_res'] = accuracy
            best_accuracy['K'] = self.k

    @staticmethod
    def found_most_major(data):
        set_data = set(data)
        count, num = 0, 0
        for i in set_data:
            if data.count(i) > count:
                count = data.count(i)
                num = i
        return num


if __name__ == '__main__':
    for k in range(1, 30):
        KNN(k).calculate()
        if best_accuracy['best_res'] == 100:
            break
    print('Accuracy = {}%\t K = {}'.format(best_accuracy['best_res'], best_accuracy['K']))


