import numpy as np
import h5py


class NaturalNetworkClass(object):
    def __init__(self, hidden_features):
        self.n_y = 1
        self.n_h = hidden_features

    def model(self, x_train, x_test, y_train, y_test, num_iterations=2000, learning_rate=0.5, verbose=False):
        self.n_x = len(x_train[0])
        params = self.initialize_with_zeros()
        params, grads, costs = self.optimize(params, X=x_train, Y=y_train, num_iterations=num_iterations,
                                             learning_rate=learning_rate, verbose=verbose)
        y_prediction_test = self.predict(params, x_test)
        y_prediction_train = self.predict(params, x_train)
        print("train accuracy = {:.3%}".format(np.mean(y_prediction_train == y_train)))
        print("test accuracy = {:.3%}".format(np.mean(y_prediction_test == y_test)))

    @staticmethod
    def sigmoid(z):
        g = 1 / (1 + np.exp(-z))
        return g

    def initialize_with_zeros(self):
        W1 = np.random.randn(self.n_h, self.n_x) * 0.01
        b1 = np.zeros(shape=(self.n_h, 1))
        W2 = np.random.randn(self.n_y, self.n_h) * 0.01
        b2 = np.zeros(shape=(self.n_y, 1))

        return {'W': [W1, W2], 'b': [b1, b2]}

    def propagate(self, params, X, Y):
        m = X.shape[0]
        W1 = params['W'][0]
        W2 = params['W'][1]

        b1 = params['b'][0]
        b2 = params['b'][1]

        Z1 = W1 @ X.T + b1
        A1 = np.tanh(Z1)

        Z2 = W2 @ A1 + b2
        A2 = self.sigmoid(Z2)

        loss = Y * np.log(A2) + (1 - Y) * np.log(1 - A2)
        cost = -1 / m * np.sum(loss)

        dJ_Z2 = A2 - Y
        dJ_W2 = 1 / m * dJ_Z2 @ A1.T
        dJ_b2 = 1 / m * np.sum(dJ_Z2, axis=1, keepdims=True)

        dJ_Z1 = W2.T @ dJ_Z2 * (1 - A1 ** 2)
        dJ_W1 = 1 / m * dJ_Z1 @ X
        dJ_b1 = 1 / m * np.sum(dJ_Z1, axis=1, keepdims=True)

        grads = {'dJ_b': [dJ_b1, dJ_b2], 'dJ_w': [dJ_W1, dJ_W2], 'dJ_z': [dJ_Z1, dJ_Z2]}

        return grads, cost

    def optimize(self, params, X, Y, num_iterations, learning_rate, verbose=False):
        costs = []
        for i in range(num_iterations):
            grads, cost = self.propagate(params, X, Y)
            W1 = params['W'][0]
            W2 = params['W'][1]
            b1 = params['b'][0]
            b2 = params['b'][1]

            W1 = W1 - learning_rate * grads['dJ_w'][0]
            b1 = b1 - learning_rate * grads['dJ_b'][0]
            W2 = W2 - learning_rate * grads['dJ_w'][1]
            b2 = b2 - learning_rate * grads['dJ_b'][1]

            if i % 5 == 0:
                costs.append(cost)

            if verbose and i % 100 == 0:
                print("Cost after iteration %i: %f" % (i, cost))

            params = {'W': [W1, W2], 'b': [b1, b2]}

        return params, grads, costs

    def predict(self, params, X):
        W1, b1, W2, b2 = params['W'][0], params['b'][0], params['W'][1], params['b'][1]
        # Forward propagation
        Z1 = W1 @ X.T + b1
        A1 = np.tanh(Z1)
        Z2 = W2 @ A1 + b2
        A2 = self.sigmoid(Z2)
        Y_prediction = (A2 >= 0.5).astype(int)

        return Y_prediction

def load_dataset():
    train_dataset = h5py.File('data/train_signs.h5', "r")
    X_train = np.array(train_dataset["train_set_x"][:])
    Y_train = np.array(train_dataset["train_set_y"][:])

    test_dataset = h5py.File('data/test_signs.h5', "r")
    X_test = np.array(test_dataset["test_set_x"][:])
    Y_test = np.array(test_dataset["test_set_y"][:])

    img_size = X_train[0].shape

    X_train_scaled = X_train.reshape(X_train.shape[0], img_size[0] * img_size[1] * img_size[2]) / 255.
    X_test_scaled = X_test.reshape(X_test.shape[0], img_size[0] * img_size[1] * img_size[2]) / 255.

    return X_train_scaled, X_test_scaled, Y_train, Y_test


if __name__ == '__main__':
    X_train, X_test, Y_train, Y_test = load_dataset()
    natural_net = NaturalNetworkClass(100)
    natural_net.model(X_train, X_test, Y_train, Y_test, num_iterations=2000, learning_rate=0.0001, verbose=True)


