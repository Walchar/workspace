from sklearn.model_selection import train_test_split
from sklearn.datasets import load_breast_cancer


cancer = load_breast_cancer()


def dec_tree_classifier():
    from sklearn.tree import DecisionTreeClassifier
    X, y, labels, features = cancer.data, cancer.target, cancer.target_names, cancer.feature_names
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)
    clf = DecisionTreeClassifier(criterion='entropy', max_depth=5, random_state=0).fit(X_train, y_train)
    print('DecisionTreeClassifier :')
    print("train accuracy = {:.3%}".format(clf.score(X_train, y_train)))
    print("test accuracy = {:.3%}".format(clf.score(X_test, y_test)))
    print('=====================')


def random_forest():
    from sklearn.ensemble import RandomForestClassifier as RFC
    X, y, labels, features = cancer.data, cancer.target, cancer.target_names, cancer.feature_names
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)
    clf = RFC(n_estimators=16, criterion='entropy', max_depth=5, random_state=0).fit(X_train, y_train)
    print('RandomForestClassifier :')
    print("train accuracy = {:.3%}".format(clf.score(X_train, y_train)))
    print("test accuracy = {:.3%}".format(clf.score(X_test, y_test)))
    print('=====================')


def gradient_boosting_decision_trees():
    from sklearn.ensemble import GradientBoostingClassifier as GBC
    X, y, labels, features = cancer.data, cancer.target, cancer.target_names, cancer.feature_names
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)
    clf = GBC(n_estimators=3, max_depth=3, random_state=0).fit(X_train, y_train)
    print('GradientBoostingClassifier :')
    print("train accuracy = {:.3%}".format(clf.score(X_train, y_train)))
    print("test accuracy = {:.3%}".format(clf.score(X_test, y_test)))
    print('=====================')


if __name__ == '__main__':
    dec_tree_classifier()
    random_forest()
    gradient_boosting_decision_trees()
