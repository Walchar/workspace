import matplotlib.pyplot as plt
import numpy as np
import random


class Kmeans(object):
    def __init__(self, n_clusters=2):
        self.n_clusters = n_clusters
        self.X = None
        self.centroids = []

    def create_data_set(self):
        from sklearn.datasets import make_classification
        self.X, _ = make_classification(n_samples=200, n_features=2, n_informative=2, n_redundant=0, random_state=0,
                                        n_clusters_per_class=1, class_sep=0.8)

    def create_centroids(self):
        for i in range(self.n_clusters):
            self.centroids.append([(random.choice(self.X[:, 0]) + random.choice(self.X[:, 0])) / 2,
                              (random.choice(self.X[:, 1]) + random.choice(self.X[:, 1])) / 2])

    def run_clustering(self):
        self.create_data_set()
        self.create_centroids()
        stop_iteration = True
        iterations = 0
        while stop_iteration:
            clustered_blobs = {i: [] for i in range(self.n_clusters)}
            for blob in self.X:
                distances = []
                for centroid in self.centroids:
                    distances.append(np.sqrt((centroid[0] - blob[0])**2 + (centroid[1] - blob[1])**2))
                clustered_blobs[distances.index(min(distances))].append([blob[0], blob[1]])
            new_centroids = []
            for key, value in clustered_blobs.items():
                value = np.array(value)
                new_centroids.append([np.mean(value[:, 0]), np.mean(value[:, 1])])
            if self.centroids != new_centroids:
                self.centroids = new_centroids
                iterations += 1
            else:
                stop_iteration = False
                for k, value in clustered_blobs.items():
                    value = np.array(value)
                    plt.scatter(value[:, 0], value[:, 1])
                    plt.plot(self.centroids[k][0], self.centroids[k][1], marker='x', markersize=14, c='black')
                    plt.title('Number of itterations: {}'.format(iterations))
                plt.show()


if __name__ == '__main__':
    K_means = Kmeans(n_clusters=3)
    K_means.run_clustering()
