from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
import nltk
import random
import json


class CreateDataSet(object):
    def __init__(self, neg, pos):
        self.neg = neg
        self.pos = pos
        self.word_features = None
        self.data_set = list()

    def extract_features(self):
        text = self.neg + '\n' + self.pos
        tokens = RegexpTokenizer(r'\w+').tokenize(str(text).lower())
        all_words = nltk.FreqDist(tokens)
        most_common_words, _ = zip(*all_words.most_common())
        stop_words = set(stopwords.words('english'))
        self.word_features = [w for w in most_common_words if w not in stop_words]

    def create_data_set(self):
        neg_reviews = str(self.neg).split('\n')
        pos_reviews = str(self.pos).split('\n')
        reviews = [(r.split(' '), 'neg') for r in neg_reviews] + [(r.split(' '), 'pos') for r in pos_reviews]
        random.shuffle(reviews)
        for rev in reviews:
            self.data_set.append(({w: w in set(rev[0]) for w in self.word_features}, rev[1]))

    def run(self):
        self.extract_features()
        self.create_data_set()
        return self.data_set


class MakeClassification(object):
    def __init__(self, data):
        self.data_set = data

    def train_test_split(self):
        split_on = int(len(self.data_set) * .8)
        x_y_train = self.data_set[:split_on]
        x_y_test = self.data_set[split_on:]
        return x_y_train, x_y_test

    def run_classifier(self):
        x_y_train, x_y_test = self.train_test_split()
        clf = nltk.NaiveBayesClassifier.train(x_y_train)
        print('Train accuracy: {}'.format(nltk.classify.accuracy(clf, x_y_train)))
        print('Test accuracy: {}'.format(nltk.classify.accuracy(clf, x_y_test)))


if __name__ == '__main__':
    neg_content = open('data/rt-polarity.neg', "r", encoding='utf-8', errors='ignore').read()
    pos_content = open('data/rt-polarity.pos', "r", encoding='utf-8', errors='ignore').read()
    data_set = CreateDataSet(neg_content, pos_content).run()
    MakeClassification(data_set).run_classifier()


