from sklearn.datasets import load_boston
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsRegressor
from sklearn.metrics.scorer import r2_score
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

n_neighbors = 3


def regression_r2(dataset):
    plt.figure()
    X_train, X_test, y_train, y_test = train_test_split(dataset.data, dataset.target, test_size=0.3)
    knn_reg = KNeighborsRegressor(n_neighbors=n_neighbors).fit(X_train, y_train)
    print("R2_score:", r2_score(y_test, knn_reg.predict(X_test)))
    return r2_score(y_test, knn_reg.predict(X_test))


def plot(dataset):
    df = pd.DataFrame(dataset.data, columns=dataset.feature_names)
    for i in range(1, 5):
        plt.subplot(2, 2, i)
        plt.scatter(df[df.columns[i]], dataset.target, c='b')
        plt.xlabel(df.columns[i])
        plt.ylabel('target')
    plt.show()


if __name__ == '__main__':
    regression_r2(load_boston())
    plot(load_boston())
