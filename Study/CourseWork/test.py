from pirc522 import RFID
import RPi.GPIO as GPIO
import time
import json


def get_id():
    while True:
        rdr.wait_for_tag()
        (error, tag_type) = rdr.request()
        if not error:
            (error, uid) = rdr.anticoll()
            if not error:
                id = str(uid).replace(', ', '.').strip('[]')
                return print(id)
            
rdr = RFID()
while True:
    get_id()
    time.sleep(2)
rdr.cleanup()