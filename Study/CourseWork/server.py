from flask import Response, Flask, jsonify
from threading import Thread
import time
import io
import threading
import picamera
import json
from pirc522 import RFID
import RPi.GPIO as GPIO

class Camera(object):
    thread = None  # background thread that reads frames from camera
    frame = None  # current frame is stored here by background thread
    last_access = 0  # time of last client access to the camera

    def initialize(self):
        if Camera.thread is None:
            # start background frame thread
            Camera.thread = threading.Thread(target=self._thread)
            Camera.thread.start()

            # wait until frames start to be available
            while self.frame is None:
                time.sleep(0)

    def get_frame(self):
        Camera.last_access = time.time()
        self.initialize()
        return self.frame

    @classmethod
    def _thread(cls):
        with picamera.PiCamera() as camera:
            camera.resolution = (1280, 960)
            stream = io.BytesIO()
            for foo in camera.capture_continuous(stream, 'jpeg',
                                                 use_video_port=True):
                # store frame
                stream.seek(0)
                cls.frame = stream.read()

                # reset stream for next frame
                stream.seek(0)
                stream.truncate()

                # if there hasn't been any clients asking for frames in
                # the last 10 seconds stop the thread
                if time.time() - cls.last_access > 10:
                    break
        cls.thread = None


app = Flask(__name__)

def gen(camera):
    """Video streaming generator function."""
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')
        
        
@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen(Camera()),mimetype='multipart/x-mixed-replace; boundary=frame')


@app.route('/data/<num>')
def index(num):
    num = int(num)
    s_data = []
    with open('/home/pi/main/data.json', 'r') as r:
        j_data = (json.load(r))
        while True:
            try:
                s_data.append(j_data[num])
                num += 1
            except:
                break
    return jsonify(s_data)


if __name__ == '__main__':
    app.run(host='192.168.1.105', port=25565, debug=True, threaded=True)

