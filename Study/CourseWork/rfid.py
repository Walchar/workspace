from pirc522 import RFID
import RPi.GPIO as GPIO
import time
import json


def get_id():
    while True:
        rdr.wait_for_tag()
        (error, tag_type) = rdr.request()
        if not error:
            (error, uid) = rdr.anticoll()
            if not error:
                return add(uid)
            
            
def add(uid):
    id = str(uid).replace(', ', '.').strip('[]')
    with open('/home/pi/main/data.json', 'r') as r:
        j_offset = (json.load(r))
    with open('/home/pi/main/data.json', 'w') as w:
        j_offset[0] = j_offset[0] + 1
        j_offset.append({'rfid':'{}'.format(id), 'case_num':1, 'offset': j_offset[0]})
        json.dump(j_offset, w, ensure_ascii=False, indent=2, sort_keys=False)
    print(j_offset)
    time.sleep(2)
    get_id()
    

rdr = RFID()
get_id()
rdr.cleanup()
