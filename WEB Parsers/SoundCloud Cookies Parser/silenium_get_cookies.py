from selenium import webdriver
from selenium.webdriver.common.by import By
import time

driver = webdriver.Firefox()


def get_sc_cookies():
    print('\n')
    time.sleep(5)
    cookies = driver.get_cookies()
    for cookie in cookies:
        print(cookie)
    return None


def sc_login():
    try:
        driver.get('https://soundcloud.com/signin?redirect_url=/stream')
        address = driver.find_element(By.XPATH, '//*[@id="content"]/div/div/div[1]/form/div/div[1]/div/div[2]/div/input')
        time.sleep(0.5)
        address.send_keys('jtm@johntracymusic.com')
        button = driver.find_element(By.CSS_SELECTOR, '#content > div > div > div.l-main > form > div > div.signinForm__step.signinForm__initial > div > button')
        button.click()
        time.sleep(2)
        password = driver.find_element(By.XPATH, '//*[@id="content"]/div/div/div[1]/form/div/div[2]/div/div[2]/div/input')
        time.sleep(0.5)
        password.send_keys('Cd1slowdn!')
        button = driver.find_element(By.CSS_SELECTOR, '#content > div > div > div.l-main > form > div > div.signinForm__step.signinForm__signin_with_password > div > button')
        button.click()
    except:
        time.sleep(2)
        print("Excepting... \nOpen your browser window")
        sc_login()
    return None


def get_browser_info():
    try:
        driver.get("http://www.thismachine.info")
        br = driver.find_element(By.XPATH, '/html/body/div[1]/div/div[1]/div[1]/div[2]/p')
        br = str(br.text).replace("/", "%2F")
        br = br.replace(" ", "%20")
        br = br.replace(";", "%3B")
        br = br.replace(",", "%2C")
        print('\n' + 'Browser info:  ' + br)
    except:
        time.sleep(2)
        print("Excepting... \nOpen your browser window")
        get_browser_info()
    return None

if __name__ == '__main__':
    get_browser_info()
    sc_login()
    get_sc_cookies()
    driver.close()