# interpreter version 3.5.3

from grab import Grab
import re

handled = []


def get_player_nickname(first_player):
    try:
        grab = Grab()

        """ Stats buffer  """
        player_name = str("")
        games_total = 0
        death_total = 0
        kills_total = 0
        vitality = int(0)
        kills_per_game = 0
        player_nicknames_buffer = ""
        all_player_id = [first_player]

        """" Add and printing statistics for one profile """
        for players in all_player_id:
            grab.go('http://stats.wogames.info/player/{}/'.format(players))

            """ Error processing """
            try:
                if grab.doc.select('//*[@id="wrap"]/div/div/div/h1').text() == "Похоже, что-то пошло не так":
                    print("player: " + str(first_player) + " not found", "\nDONE!!!\n" + "==================================================")
                    return
            except:
                pass

            """ Getting all profile id and writing into a variable: all_player_id """
            if players == first_player:
                href = grab.doc.tree.xpath('//*[@id="wrap"]/div/div/div[1]/a/@href')
                for i in href:
                    player_nicknames_buffer += i
                players_id_buffer = re.findall(r'\d+', player_nicknames_buffer)
                for i in players_id_buffer:
                    handled.append(i)
                    all_player_id.append(i)

            """ Calculations """
            try:
                player_name += str("\n " + (grab.doc.select('//*[@id="wrap"]/div/div/div[1]/h3').text()).split("Информация по игроку ")[1])
                games_total += int(grab.doc.select('//*[@id="wrap"]/div/div/div[2]/div[1]/div/table/tbody/tr[1]/td').text())
                death_total += int(grab.doc.select('//*[@id="wrap"]/div/div/div[2]/div[1]/div/table/tbody/tr[2]/td').text())
                kills_total += int(grab.doc.select('//*[@id="wrap"]/div/div/div[2]/div[1]/div/table/tbody/tr[7]/td').text())
                vitality = int((games_total - death_total) / games_total * 100)
                kills_per_game = kills_total / games_total
            except:
                pass
        print("Также известен как: " + str(player_name) + "\n___________________________"  "\nИгр всего: " + str(games_total), "\nСмертей всего: " + str(death_total), "\nЖивучесть: " + str(vitality) + "%", "\nФрагов всего: " + str(kills_total), "\nФрагов за игру: " + str(kills_per_game), "\nDONE!!!\n" + "==================================================")
        return
    except:
        print("except: " + str(first_player))
        get_player_nickname(first_player)
        return


""" Parsing all profiles """
if __name__ == '__main__':
    for i in range(1, 3400):
        print("Player: " + str(i) + " of 3400")
        if str(i) not in handled:
            get_player_nickname(first_player=i)
            handled.append(i)
        else:
            print("This id was added before.\n")


