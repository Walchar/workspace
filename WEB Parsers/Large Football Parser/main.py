from selenium import webdriver
from selenium.webdriver.common.by import By as by
import selenium
import time
import datetime
import json
import math as m
import re
import os
import difflib
import logging


def create_log(country, message, level):
    message = message.replace('(Session info: chrome=75.0.3770.142)', '').replace('\n', '').encode("utf-8")
    if level == 'info':
        logging.info("{} - {}".format(country, message))
    else:
        logging.error("{} - {}".format(country, message))


class Main(object):
    def __init__(self):
        pass

    def site_1(self):
        driver.get('http://supabets.com.gh/Sport/Groups.aspx?IDSport=468&Antepost=0')
        driver.find_element(by.XPATH, '//*[@id="s_w_PC_op_Label1"]').click()
        ret = 0
        cal = day[5]
        if day[5][0] == '0':
            cal = day[5][1]
        for i in range(1, 7):
            if ret > 0:
                break
            for j in range(1, 8):
                try:
                    path = '/html/body/form/div[3]/div[2]/div/table/tbody/tr/td[1]/div[3]/div[2]/div/div[1]/div/div/' \
                           'div/div[2]/div[2]/div/div[9]/table/tbody/tr/td/table/tbody[2]/tr[{}]/td[{}]/a/span'
                    res = driver.find_element(by.XPATH, path.format(i, j))
                except Exception as e:
                    create_log('Supabets: ghana, south africa', 'calendar: {}'.format(e), 'error')
                    continue
                if res.text == str(cal):
                    ret += 1
                    res.click()
                    driver.find_element(by.XPATH, '//*[@id="s_w_PC_op_cldOdds_BtnCalendar"]').click()
                    time.sleep(4)
                    break
                continue
        num = 1
        exc = 0
        while True:
            num += 1
            try:
                driver.find_element(by.XPATH, '//*[@id="S468_0"]/table/tbody/tr[{}]/td[1]/a'.format(num)).click()
                driver.find_element(by.XPATH, '//*[@id="S468_0"]/table/tbody/tr[{}]/td[2]/a'.format(num)).click()
                time.sleep(2.5)
                exc = 0
            except Exception as e:
                create_log('Supabets: ghana, south africa', 'countries_list: {}'.format(e), 'error')
                exc += 1
                if exc == 15:
                    break
                continue
            group = 1
            while True:
                try:
                    gr_path = '//*[@id="divMainEvents"]/div[{}]/div[1]/span'.format(group)
                    gr = driver.find_element(by.XPATH, gr_path).text
                    gr = str(gr).split('-', 1)
                    div = 1
                    while True:
                        try:
                            res_path = '//*[@id="divMainEvents"]/div[{}]/div[2]/div[{}]'.format(group, div)
                            res = str(driver.find_element(by.XPATH, res_path).text).split('\n')

                            if len(res) < 4:
                                group += 1
                                break
                            odds = [res[9].split('/'), res[6].split('/'), res[8].split('/')]
                            odds[0] = float("%.2f" % (int(odds[0][0]) / int(odds[0][1]) + 1))
                            odds[1] = float("%.2f" % (int(odds[1][0]) / int(odds[1][1]) + 1))
                            odds[2] = float("%.2f" % (int(odds[2][0]) / int(odds[2][1]) + 1))
                            odds = [str(elem) + '0' if len(str(elem)) == 3 else str(elem) for elem in odds]
                            json_load('Supabets', 'ghana', res[1], res[2].split(' ')[0],
                                      res[3], res[4], gr[1], gr[0], odds[0], odds[1], odds[2])
                            json_load('Supabets', 'south africa', res[1], res[2].split(' ')[0],
                                      res[3], res[4], gr[1], gr[0], odds[0], odds[1], odds[2])
                            json_load('Supabets', 'zambia', res[1], res[2].split(' ')[0],
                                      res[3], res[4], gr[1], gr[0], odds[0], odds[1], odds[2])
                            div += 1
                        except Exception as e:
                            create_log('Supabets: ghana, south africa', 'getting_data: {}'.format(e), 'error')
                            group += 1
                            break
                except Exception as e:
                    create_log('Supabets: ghana, south africa', 'getting_SuperGroup: {}'.format(e), 'error')
                    break
            driver.get('http://supabets.com.gh/Sport/Groups.aspx?IDSport=468&Antepost=0')

    def site_2(self):
        driver.get('https://sports.premierbet.com/en?timestamp=1548529753&removet=1&lang=en')
        link = '/html/body/div/div/div/div/div[10]/div/div/div[4]/div/div/div/'
        time.sleep(2)
        driver.find_element(by.XPATH, '//*[@id="day1"]').click()
        time.sleep(2)
        gr_run, gr_iteration = True, 2
        while gr_run:
            try:
                gr = str(driver.find_element(by.XPATH, link + 'div[{}]/div[1]/div[1]/div[5]'
                                                              .format(gr_iteration)).text).split(' -> ')
                gr = {'Sgroup': gr[0], 'league': gr[1]}
                match_run, match_iteration = True, 3
                while match_run:
                    try:
                        res = str(driver.find_element(by.XPATH, link + 'div[{}]/div[2]/div[2]/div[{}]'
                                                      .format(gr_iteration, match_iteration)).text).split('\n')
                        tm = str(driver.find_element(by.XPATH, link + 'div[{}]/div[2]/div[2]/div[{}]/div[2]'
                                                     .format(gr_iteration, match_iteration))
                                 .get_attribute('data-forheader')).split(' vs ')
                        json_load('Premier Bet', 'ghana', res[0], res[1].split('-')[0], tm[0], tm[1],
                                  gr['league'], gr['Sgroup'], res[5], res[3], res[4])
                        match_iteration += 1
                    except Exception as e:
                        create_log('Premier Bet: ghana', 'getting_data: {}'.format(e), 'error')
                        match_run = False
                        gr_iteration += 1
            except Exception as e:
                create_log('Premier Bet: ghana', 'getting_group: {}'.format(e), 'error')
                gr_run = False
                pass

    def site_3(self):
        driver.get('https://www.safaribetting.com/bets/date/{}.{}.{}'.format(day[5], day[3], day[1]))
        link = '//*[@id="betsTable"]/'
        div = 1
        while True:
            try:
                gr = str(driver.find_element(by.XPATH, link + 'div[{}]/div[1]'.format(div)).text).split(' - ')
                if gr[0] == 'Tennis' or gr[0] == 'Basketball' or gr[0] == 'Icehockey':
                    break
                mat = 2
                while True:
                    try:
                        res = driver.find_element(by.XPATH, link + 'div[{}]/div[2]/div[{}]'.format(div, mat)).text
                        res = str(res).split('\n')
                        t = res[0].split(' ')[1]
                        t_m = res[2].split(' - ')
                        json_load('Safaribet', 'ghana', t, day[5], t_m[0], t_m[1], gr[2], gr[1], res[7], res[5], res[6])
                        mat += 1
                    except Exception as e:
                        create_log('Safaribet: ghana', 'getting_data: {}'.format(e), 'error')

                        div += 1
                        break
            except Exception as e:
                create_log('Safaribet: ghana', 'getting_group: {}'.format(e), 'error')
                break

    def site_4(self):
        driver.get('http://supabets.com.gh/Sport/Groups.aspx?IDSport=468&Antepost=0')
        driver.find_element(by.XPATH, '//*[@id="s_w_PC_op_Label1"]').click()
        ret = 0
        cal = day[5]
        if day[5][0] == '0':
            cal = day[5][1]
        for i in range(1, 7):
            if ret > 0:
                break
            for j in range(1, 8):
                try:
                    path = '/html/body/form/div[3]/div[2]/div/table/tbody/tr/td[1]/div[3]/div[2]/div/div[1]/div/div/' \
                           'div/div[2]/div[2]/div/div[9]/table/tbody/tr/td/table/tbody[2]/tr[{}]/td[{}]/a/span'
                    res = driver.find_element(by.XPATH, path.format(i, j))
                except Exception as e:
                    create_log('Eazibet: ghana', 'calendar: {}'.format(e), 'error')
                    continue
                if res.text == str(cal):
                    ret += 1
                    res.click()
                    driver.find_element(by.XPATH, '//*[@id="s_w_PC_op_cldOdds_BtnCalendar"]').click()
                    break
                continue
        num = 1
        exc = 0
        while True:
            num += 1
            try:
                driver.find_element(by.XPATH, '//*[@id="S468_0"]/table/tbody/tr[{}]/td[1]/a'.format(num)).click()
                driver.find_element(by.XPATH, '//*[@id="S468_0"]/table/tbody/tr[{}]/td[2]/a'.format(num)).click()
                time.sleep(2.5)
                exc = 0
            except Exception as e:
                create_log('Eazibet: ghana', 'countries_list: {}'.format(e), 'error')
                exc += 1
                if exc == 15:
                    break
                continue
            group = 1
            while True:
                try:
                    gr_path = '//*[@id="divMainEvents"]/div[{}]/div[1]/span'.format(group)
                    gr = driver.find_element(by.XPATH, gr_path).text
                    gr = str(gr).split('-', 1)
                    div = 1
                    while True:
                        try:
                            res_path = '//*[@id="divMainEvents"]/div[{}]/div[2]/div[{}]'.format(group, div)
                            res = str(driver.find_element(by.XPATH, res_path).text).split('\n')

                            if len(res) < 4:
                                group += 1
                                break
                            odds = [res[9].split('/'), res[6].split('/'), res[8].split('/')]
                            odds[0] = float("%.2f" % (int(odds[0][0]) / int(odds[0][1]) + 1))
                            odds[1] = float("%.2f" % (int(odds[1][0]) / int(odds[1][1]) + 1))
                            odds[2] = float("%.2f" % (int(odds[2][0]) / int(odds[2][1]) + 1))
                            odds = [str(elem) + '0' if len(str(elem)) == 3 else str(elem) for elem in odds]
                            json_load('Eazibet', 'ghana', res[1], res[2].split(' ')[0],
                                      res[3], res[4], gr[1], gr[0], odds[0], odds[1], odds[2])
                            div += 1
                        except Exception as e:
                            create_log('Eazibet: ghana', 'getting_data: {}'.format(e), 'error')
                            group += 1
                            break
                except Exception as e:
                    create_log('Eazibet: ghana', 'getting_SuperGroup: {}'.format(e), 'error')
                    break
            driver.get('http://supabets.com.gh/Sport/Groups.aspx?IDSport=468&Antepost=0')

    def site_5(self):
        driver.get('https://www.sunbet.com.gh/#/sport/?type=0&competition=566&sport=1&region=20001&game=12081829')
        time.sleep(8)
        link = '/html/body/div[1]/div[2]/div/div/div/div/div/div[1]/div/div[1]/div[2]/div[1]/ul/'
        driver.find_element(by.XPATH, link + 'li[2]/i').click()
        driver.find_element(by.XPATH, link + 'li[1]/div/p/span').click()
        time.sleep(1)
        driver.find_element(by.XPATH, link + 'li[1]/div/div/ul/li[7]/span').click()
        time.sleep(5)
        for i in range(2):
            driver.find_element(by.XPATH, '/html/body/div[1]/div[2]/div/div/div/div/div/div[1]/div/div[2]/div[2]/'
                                          'include-template[3]/div/ul/li[3]/ul/li[2]/div[1]').click()
        link = '/html/body/div[1]/div[2]/div/div/div/div/div/div[1]/div/div[2]/' \
               'div[2]/include-template[3]/div/ul/li[3]/ul/li[2]/div[2]/div/'
        time.sleep(2)
        div = 1
        while True:
            try:
                driver.find_element(by.XPATH, link + 'div[{}]/div[1]'.format(div)).click()
                driver.find_element(by.XPATH, link + 'div[{}]/div[1]'.format(div)).location_once_scrolled_into_view
                time.sleep(1.5)
                mat = 1
                while True:
                    try:
                        driver.find_element(by.XPATH, link + 'div[{}]/div[2]/div[1]/'
                                                             'div[1]/div[{}]'.format(div, mat)).click()
                        mat += 1
                    except Exception as e:
                        create_log('Sunbet: ghana', 'getting_league_data: {}'.format(e), 'error')
                        div += 1
                        break
            except Exception as e:
                create_log('Sunbet: ghana', 'getting_group_data: {}'.format(e), 'error')
                break
        div = 2
        link = '/html/body/div[1]/div[2]/div/div/div/div/div/div[2]/div/div[1]/div[2]/'
        while True:
            try:
                gr = driver.find_element(by.XPATH, link + 'div[{}]/div[1]'.format(div)).text
                gr = str(gr).split('FOOTBALL. ')[1].split(' (POSSIBLE FORMAT CHANGE)')[0].split(' - ')
                dy = driver.find_element(by.XPATH, link + 'div[{}]/div[2]/div[1]'.format(div)).text
                dy = str(dy).split('.')[0]
                tl = 2

                while True:
                    try:
                        res = str(driver.find_element(by.XPATH, link + 'div[{}]/div[2]/'
                                                                       'div[{}]'.format(div, tl)).text).split('\n')
                        if len(res) != 6:
                            tl += 1
                            continue
                        t_m = res[1].split(' vs ')
                        json_load('Sunbet', 'ghana', res[0], dy, t_m[0], t_m[1], gr[1], gr[0], res[5], res[3], res[4])
                        json_load('Sunbet', 'zambia', res[0], dy, t_m[0], t_m[1], gr[1], gr[0], res[5], res[3], res[4])

                        tl += 1
                    except Exception as e:
                        create_log('Sunbet: ghana', 'getting_data: {}'.format(e), 'error')
                        div += 1
                        break
            except Exception as e:
                create_log('Sunbet: ghana', 'getting_group: {}'.format(e), 'error')
                break

    def site_6(self):
        driver.get('https://www.soccabet.com/#/')
        time.sleep(2)
        match = 1
        path = '//*[@id="Soccer-tab"]/div/'
        allow = True
        while allow:
            try:
                res = str(driver.find_element(by.XPATH, path + 'article[{}]'.format(match)).text).split('\n')
                tm = res[4].split(' v ')
                json_load('Soccabet', 'ghana', res[1], day[5], tm[0], tm[1], res[2], res[3], res[7], res[5], res[6])
                match += 1
            except Exception as e:
                create_log('Soccabet: ghana', 'getting_data: {}'.format(e), 'error')
                try:
                    driver.find_element(by.XPATH, path + 'div[2]').find_element(by.CLASS_NAME, 'show-more').click()
                except Exception as e:
                    create_log('Soccabet: ghana', 'updating_page: {}'.format(e), 'error')
                    allow = False

    def site_7(self):
        driver.get('https://www.betfair.com/sport/football')
        try:
            driver.find_element(by.XPATH, '/html/body/div/div/div/div/div/div/div/div/div/'
                                          'table/tbody/tr/td[6]/div/div').click()
            driver.find_element(by.XPATH, '/html/body/div/div/div/div/div/div/div/div/div/'
                                          'table/tbody/tr/td[6]/div/div[2]/div/ul/li/a').click()
        except Exception as e:
            create_log('Betfair: ghana', 'changing_country: {}'.format(e), 'error')
        time.sleep(5)
        li = 1
        if 'Today' in driver.find_element(by.XPATH, '/html/body/div[2]/div/div/div/div/div[2]'
                                                    '/div/div/div/div/div[5]/div/ul/li[1]/div').text:
            block = 1
        else:
            block = 2
        time.sleep(5)
        while True:
            try:
                match = '/html/body/div[2]/div/div/div/div/div[2]/div/div/div/div/div[5]/div/ul/li[{}]/ul/li[{}]/div/'.format(block, li)
                tm = str(driver.find_element(by.XPATH, match + 'div').text).split(' ')
                if 'soon' in tm:
                    li += 1
                    continue
                if 'Starting' in tm[0]:
                    ct = time.ctime().split(' ')[4]
                    mit = str(int(ct.split(':')[1]) + int(tm[2].strip('′')))
                    if int(mit) >= 60:
                        if int(mit) - 60 < 10:
                            tm = (str(int(ct.split(':')[0]) + 1) + ':0' + str(int(mit) - 60)).split('#')
                        else:
                            tm = (str(int(ct.split(':')[0]) + 1) + ':' + str(int(mit) - 60)).split('#')
                    else:
                        tm = (str(ct.split(':')[0]) + ':' + mit).split('#')
                bets = str(driver.find_element(by.XPATH, match + 'div[2]/div[2]').text).split('\n')
                if 'SUSPENDED' in bets or len(bets) != 3:
                    li += 1
                    continue
                teams = str(driver.find_element(by.XPATH, match + 'div[3]/div/a').text).split('\n')
                json_load('Betfair', 'ghana', tm[0], day[5], teams[0], teams[1], None, None, bets[2], bets[0], bets[1])
                li += 1
            except Exception as e:
                create_log('Betfair: ghana', 'getting_data: {}'.format(e), 'error')
                break

    def site_8(self):
        driver.get('https://www.betolimp.com/en/sports/')
        driver.find_element(by.XPATH, '//*[@id="filter-select"]').click()
        driver.find_element(by.XPATH, '//*[@id="filter-select"]/option[5]').click()
        link = '//*[@id="champforms"]/div[2]/div[2]/div[1]/'
        time.sleep(3)
        div = 1
        while True:
            try:
                elem = driver.find_element(by.XPATH, link + 'div[{}]/div[2]/input'.format(div))
                driver.execute_script("arguments[0].scrollIntoView(true);", elem)
                driver.execute_script("arguments[0].click();", elem)
                div += 1
            except Exception as e:
                create_log('Betolimp: ghana', 'scrolling_list: {}'.format(e), 'error')
                driver.execute_script("arguments[0].scrollIntoView();", driver.find_element(by.TAG_NAME, "header"))
                driver.find_element(by.XPATH, '//*[@id="createcoup"]').click()
                time.sleep(5)
                div = 1
                break
        link = '/html/body/div[5]/article/div/div/div[2]/div[2]/'
        gr = None
        while True:
            try:
                if driver.find_element(by.XPATH, link + 'div[{}]'.format(div)).get_attribute('class') != 'ch_list':
                    gr = str(driver.find_element(by.XPATH, link + 'div[{}]'.format(div)).text)
                else:
                    mat = 1
                    while True:
                        try:
                            res = driver.find_element(by.XPATH, link + 'div[{}]/div[{}]'.format(div, mat)).text
                            res = str(res).split('\n')
                            tm = res[0].split(' ', 3)
                            t_m = tm[3].split(' - ')
                            path = link + 'div[{}]/div[{}]/div[1]/div[2]/'.format(div, mat)
                            bets = [driver.find_element(by.XPATH, path + 'div[{}]/span[2]'.format(i)).text for i in range(1, 4)]
                            if '' not in bets:
                                json_load('BetOlimp', 'ghana', tm[2], tm[1], t_m[0],
                                          t_m[1], gr, None, bets[2], bets[0], bets[1])
                                json_load('BetOlimp', 'zambia', tm[2], tm[1], t_m[0],
                                          t_m[1], gr, None, bets[2], bets[0], bets[1])
                            mat += 1
                        except Exception as e:
                            create_log('Betolimp: ghana', 'getting_data: {}'.format(e), 'error')
                            break
                div += 1
            except Exception as e:
                create_log('Betolimp: ghana', 'getting_group: {}'.format(e), 'error')
                break

    def site_9(self):
        driver.get('https://sports.betway.com/en/sports/cpn/soccer/9')
        driver.find_element(by.XPATH, '/html/body/div/div/div[4]/div/div/div[2]/div').click()
        link = '/html/body/div/div/div[2]/div/div[1]/div/div[2]/div[4]/div/div[2]/div/div[2]/div/div/div[1]/div[2]/div/'

        div = 1
        mat = 1
        while True:
            try:
                lg = driver.find_element(by.XPATH, link + 'div[{}]/div[1]/div[2]/div[1]'.format(div)).text
                gr = driver.find_element(by.XPATH, link + 'div[{}]/div[1]/div[2]/div[2]'.format(div)).text
                try:
                    driver.find_element(by.XPATH, link + 'div[{}]/div[2]/div[1]/div[{}]'.format(div, mat))
                except Exception as e:
                    create_log('Betway: ListOf', 'Element_checker: {}'.format(e), 'error')
                    driver.find_element(by.XPATH, link + 'div[{}]'.format(div)).click()
                    time.sleep(1)
                while True:
                    try:
                        res = str(driver.find_element(by.XPATH, link + 'div[{}]/div[2]/div[1]/div[{}]'
                                                      .format(div, mat)).text).split('\n')
                        if res[1] != '-':
                            t_m = res[1].split(' - ')
                            country = ['kenya', 'uganda', 'zambia', 'ghana', 'south africa']
                            for i in country:
                                json_load('Betway', i, res[0], day[5], t_m[0],
                                          t_m[1], lg, gr, res[4], res[2], res[3])
                        mat += 1
                    except Exception as e:
                        create_log('Betway: ListOf', 'getting_data: {}'.format(e), 'error')
                        mat = 1
                        div += 1
                        break
            except Exception as e:
                create_log('Betway: ListOf', 'getting_group: {}'.format(e), 'error')
                break

    def site_10(self):
        driver.get('https://www.winnabets.com/#/init')
        time.sleep(3)
        li = 1
        while True:
            try:
                match = '/html/body/div[4]/div[2]/div[3]/div[6]/div[3]/div/div[3]/div[2]/ul/li[{}]'.format(li)
                res = (str(driver.find_element(by.XPATH, match).text).split('\n'))
                if len(res) >= 8:
                    if int(res[1].split(' ')[0]) <= int(day[5]):
                        tm = str(driver.find_element(by.XPATH, match + '/div[6]/div/div[2]').text).split(' v ')
                        json_load('WinnaBet', 'ghana', res[0], res[1].split(' ')[0],
                                  tm[0], tm[1], None, None, res[5], res[3], res[4])
                    else:
                        break
                li += 1
            except Exception as e:
                create_log('WinnaBet: ghana', 'getting_data: {}'.format(e), 'error')
                m_markets = '//*[@id="mainContents-inner"]/div[3]/div[6]/div[3]/div/div[3]/div[2]/div[3]/div[{}]'
                if driver.find_element(by.XPATH, m_markets.format(1)).text == 'Show more markets':
                    driver.find_element(by.XPATH, m_markets.format(1)).click()
                else:
                    driver.find_element(by.XPATH, m_markets.format(2)).click()

    def site_11(self):
        driver.get('https://sports2.premierbet.com/en?timestamp=1565131153&'
                   'removet=1&lang=en&currencyCode=EUR&brandId=11')
        time.sleep(2)
        driver.find_element(by.XPATH, '//*[@id="day1"]').click()
        time.sleep(2)
        path = "/html/body/div/div/div/div/div[9]/div/div/div[4]/div/div/div/"
        div_gr = 2
        while True:
            try:
                gr = str(driver.find_element(by.XPATH, path + 'div[{}]/div[1]/div[1]/'
                                                              'div[5]'.format(div_gr)).text).split(' -> ')
                match = 3
                while True:
                    try:
                        res = str(driver.find_element(by.XPATH, path + 'div[{}]/div[2]/div[2]/div'
                                                                       '[{}]'.format(div_gr, match)).text).split('\n')
                        date = res[1].split('-')[0]
                        home = driver.find_element(by.XPATH, path + 'div[{}]/div[2]/div[2]/div[{}]/div[2]/a/div/'
                                                                    'span[1]'.format(div_gr, match)).text
                        away = driver.find_element(by.XPATH, path + 'div[{}]/div[2]/div[2]/div[{}]/div[2]/a/div/'
                                                                    'span[2]'.format(div_gr, match)).text
                        json_load('Premier Bet', 'tanzania', res[0], date, home, away, gr[1],
                                  gr[0], res[5], res[3], res[4])
                        match += 1
                    except Exception as e:
                        create_log('PremierBet: tanzania', 'getting_data: {}'.format(e), 'error')
                        div_gr += 1
                        break
            except Exception as e:
                create_log('PremierBet: tanzania', 'getting_group: {}'.format(e), 'error')
                break

    def site_12(self):
        driver.get('https://m-bet.co.tz')
        tr = 1
        while True:
            try:
                res = str(driver.find_element(by.XPATH, '//*[@id="fixturesTable"]/tbody[2]/'
                                                        'tr[{}]'.format(tr)).text).split('\n')
                for i in range(1, 4):
                    res.append(driver.find_element(by.XPATH, '//*[@id="fixturesTable"]/tbody[2]/tr[{}]/td[3]/div/'
                                                             'div[2]/input[{}]'.format(tr, i)).get_attribute('value'))
                tr += 1
                t_m = res[2].split(' vs ')
                json_load('M-Bet', 'tanzania', res[0], day[5], t_m[0], t_m[1], res[1], None, res[8], res[6], res[7])
            except Exception as e:
                create_log('M-Bet: tanzania', 'getting_data: {}'.format(e), 'error')
                break

    def site_13(self):
        driver.get('http://princessbet.co.tz/sports')
        driver.find_element(by.XPATH, '//*[@id="Sub_SportsBetting"]/li[1]').click()
        time.sleep(5)
        div = 2
        while True:
            path = '//*[@id="bodyContainer"]/div[3]/ng-repeat/div[{}]'.format(div)
            try:
                gr = driver.find_element(by.XPATH, path + '/div[1]').text
                lg_div = 1
                while True:
                    try:
                        lg = driver.find_element(by.XPATH, path + '/div[2]/div[{}]/div[1]'.format(lg_div)).text

                        mat = 1
                        while True:
                            try:
                                res = str(driver.find_element(by.XPATH, path + '/div[2]/div[{}]/div[3]/div[{}]'.
                                                              format(lg_div, mat)).text).split('\n')
                                if res[1].split('/')[0] != day[5]:
                                    continue
                                json_load('Princess Bet', 'tanzania', res[0], day[5], res[2], res[3],
                                          lg, gr, res[6], res[4], res[5])
                                mat += 1
                            except Exception as e:
                                create_log('PrincessBet: tanzania', 'getting_data: {}'.format(e), 'error')
                                lg_div += 1
                                break
                    except Exception as e:
                        create_log('PrincessBet: tanzania', 'getting_league: {}'.format(e), 'error')
                        div += 1
                        break
            except Exception as e:
                create_log('PrincessBet: tanzania', 'getting_group: {}'.format(e), 'error')
                break

    def site_14(self):
        driver.get('https://thronebettanzania.com/daily-update-fixtures/')
        pdf = str(driver.find_element(by.XPATH, '/html/body/div[3]/div/div/div/div/iframe').get_attribute('src'))
        driver.get(pdf)
        time.sleep(4)
        tm = time.ctime().split(' ')[0].lower()
        games = ['BASKETBALL', 'TENNIS', 'VOLLEYBALL', 'BASEBALL']
        page = 1
        while True:
            try:
                driver.find_element(by.XPATH, '//*[@id="viewer"]/div[{}]'.format(page)).click()
                time.sleep(0.5)
                dt = str(driver.find_element(by.XPATH, '//*[@id="viewer"]/div[{}]/'
                                                       'div[2]'.format(page)).text).split('\n')
                page += 1
                for i in range(len(dt)):
                    if dt[i] in games:
                        return
                    elif dt[i] == tm:
                        lg = dt[i + 3].replace(' -', '')
                        json_load('Throne Bet', 'tanzania', dt[i + 1], day[5], dt[i + 4], dt[i + 5], lg,
                                None, dt[i + 8], dt[i + 6], dt[i + 7])
            except Exception as e:
                create_log('Thronebet: tanzania', 'getting_data: {}'.format(e), 'error')
                break

    def site_15(self):
        driver.get('https://web.bet9ja.com/Sport/Default.aspx')
        driver.find_element(by.XPATH, '//*[@id="panelOdds_NE"]/div[2]/div[10]/div[3]/span').click()
        time.sleep(4)
        path = '/html/body/form/div[7]/div[1]/div[2]/div/table/tbody/tr/td[2]/div/div/div/div[16]/div/div[3]/'
        div = 1
        gr = str()
        while True:
            try:
                data = driver.find_element(by.XPATH, path + 'div[{}]'.format(div))
                driver.execute_script("arguments[0].scrollIntoView(true);", data)
                t_m = str()
                tm = str()
                odds = list()
                mat = 1
                while True:
                    try:
                        elem = data.find_element(by.XPATH, 'div[{}]'.format(mat))
                        attr = elem.get_attribute('class')
                        if attr == "Time":
                            t_m = str(elem.text).split(' ')
                        if attr == "EventParent ng-binding":
                            gr = elem.text if elem.text != '' else gr
                        if attr == "Event ng-binding":
                            tm = str(elem.text).split(' - ')
                        if attr == "odds":
                            buff = str(elem.text).split('\n')
                            odds = [buff[6], buff[2], buff[4]] if buff[0] == "1X2" else [buff[2], buff[0], buff[1]]
                        mat += 1
                    except Exception as e:
                        create_log('Bet9ja: nigeria', 'searching_data: {}'.format(e), 'error')
                        div += 1
                        break
                json_load('Bet9ja', 'nigeria', t_m[0], t_m[1], tm[0], tm[1], gr, None, odds[0], odds[1], odds[2])
            except Exception as e:
                create_log('Bet9ja: nigeria', 'getting_data: {}'.format(e), 'error')
                break

    def site_16(self):
        driver.get('https://merrybet.com/sports')
        driver.find_element(by.XPATH, '//*[@id="searchForm"]/fieldset[2]/select').click()
        driver.find_element(by.XPATH, '//*[@id="searchForm"]/fieldset[2]/select/option[6]').click()
        time.sleep(5)
        driver.find_element(by.XPATH, '/html/body/div[3]/div[2]/div[4]/div/div/div/div/div/div/'
                                      'div[1]/div/div[2]/div').click()
        time.sleep(2)
        path = '/html/body/div[3]/div[2]/div[4]/div/div/div/div/div/div/div[3]/div[2]/div/div/div/'
        exc = 0
        div = 1
        div_lg = 1
        while exc < 15:
            try:
                gr = str(driver.find_element(by.XPATH, path + 'div[{}]/div/div/div/div[{}]'
                                                              '/div[1]/div'.format(div, div_lg)).text).split(' - ')
                exc = 0
                mat = 1
                while True:
                    try:
                        res = str(driver.find_element(by.XPATH, path + 'div[{}]/div/div/div/div[{}]/div[2]'
                                                      .format(div, div_lg)).find_element(by.CLASS_NAME, 'panel-body')
                                  .find_element(by.XPATH, 'div[{}]'.format(mat)).text).split('\n')
                        if len(res) < 15:
                            tm = res[5].split(' - ')
                            json_load('MerryBet', 'nigeria', res[1], res[0].split('.')[0], tm[0], tm[1],
                                      gr[1], gr[0], res[8], res[6], res[7])
                        mat += 1
                    except Exception as e:
                        create_log('MerryBet: nigeria', 'getting_data: {}'.format(e), 'error')
                        div_lg += 1
                        break
            except Exception as e:
                create_log('MerryBet: nigeria', 'getting_group: {}'.format(e), 'error')
                exc += 1
                div += 1
                div_lg = 1

    def site_17(self):
        driver.get('https://www.winnersgoldenbet.com/search/hour/24')
        time.sleep(3)
        table = '/html/body/div[1]/div[2]/div[2]/div/div/div[3]/partial[2]/div/div/div/div[2]/'
        run_country, country, league = True, 1, 1
        group = str()
        while run_country:
            try:
                break_point = driver.find_element(by.XPATH, table + 'div[{}]'.format(country)).text
            except Exception as e:
                create_log('Winners Golden Bet: nigeria', 'break_point: {}'.format(e), 'error')
                break
            try:
                elem = driver.find_element(by.XPATH, table + 'div[{}]/div[{}]'
                                           .format(country, league)).get_attribute('class')
                if elem == 'league-header odd-names-row':
                    group = str(driver.find_element(by.XPATH, table + 'div[{}]/div[{}]'
                                                    .format(country, league)).text).split(' - ')
                    group[1] = group[1].split('\n')[0]
                elif elem == 'eventListLeagueEventsListPartial league-panel list-type-league' \
                             ' expand-limited' and group[1] != 'Outrights':
                    run_match, match = True, 1
                    while run_match:
                        try:
                            check = driver.find_element(by.XPATH, table + 'div[{}]/div[{}]/ul/li[{}]'
                                                        .format(country, league, match))
                            if check.get_attribute('class') == 'col-all-12 single-event ':
                                res = driver.find_element(by.XPATH, table + 'div[{}]/div[{}]/ul/li[{}]'
                                                          .format(country, league, match)).text
                                res = str(res).split('\n')
                                t_m = res[0].split(' ')
                                tm = res[1].split(' - ')
                                if 'Today' in t_m[0]:
                                    json_load('Winners Golden Bet', 'nigeria', t_m[1], day[5], tm[0], tm[1], group[1],
                                              group[0], res[4], res[2], res[3])
                            match += 1
                        except Exception as e:
                            create_log('Winners Golden Bet: nigeria', 'getting_data: {}'.format(e), 'error')
                            run_match = False
                league += 1
            except Exception as e:
                create_log('Winners Golden Bet: nigeria', 'getting_group: {}'.format(e), 'error')
                country += 1
                league = 1

    def site_18(self):
        driver.get('https://www.lionsbet.com/ng/search/hour/24')
        time.sleep(5)
        driver.find_element(by.XPATH, '/html/body/div[1]/div[2]/div[3]/div/div/'
                                      'div[2]/partial/div/ul/li[1]/button').click()
        time.sleep(2)
        table = '/html/body/div[1]/div[2]/div[3]/div/div/div[3]/partial[2]/div/div/div/div[2]/'
        run_country, country, league = True, 1, 1
        group = str()
        while run_country:
            try:
                break_point = driver.find_element(by.XPATH, table + 'div[{}]'.format(country)).text
            except Exception as e:
                create_log('Lionsbet: nigeria', 'break_point: {}'.format(e), 'error')
                break
            try:
                elem = driver.find_element(by.XPATH, table + 'div[{}]/div[{}]'
                                           .format(country, league)).get_attribute('class')
                if elem == 'league-header odd-names-row':
                    group = str(driver.find_element(by.XPATH, table + 'div[{}]/div[{}]'
                                                    .format(country, league)).text).split(' - ')
                    group[1] = group[1].split('\n')[0]
                elif elem == 'eventListLeagueEventsListPartial league-panel list-type-league' \
                             ' expand-limited' and group[1] != 'Outrights':
                    run_match, match = True, 1
                    while run_match:
                        try:
                            check = driver.find_element(by.XPATH, table + 'div[{}]/div[{}]/ul/li[{}]'
                                                        .format(country, league, match))
                            if check.get_attribute('class') == 'col-all-12 single-event ':
                                res = str(driver.find_element(by.XPATH, table + 'div[{}]/div[{}]/ul/li[{}]'
                                                              .format(country, league, match)).text).split('\n')
                                t_m = res[0].split(' ')
                                tm = res[1].split(' - ')
                                if 'Today' in t_m[0]:
                                    json_load('Lions Bet', 'nigeria', t_m[1], day[5], tm[0], tm[1], group[1],
                                              group[0], res[4], res[2], res[3])
                            match += 1
                        except Exception as e:
                            create_log('Lionsbet: nigeria', 'getting_data: {}'.format(e), 'error')
                            run_match = False
                league += 1
            except Exception as e:
                create_log('Lionsbet: nigeria', 'getting_group: {}'.format(e), 'error')
                country += 1
                league = 1

    def site_19(self):
        driver.get('https://accessbet.com/Sport/sport?r=1#')
        time.sleep(3)
        driver.find_element(by.XPATH, '//*[@id="spo_1"]').click()
        time.sleep(2)
        driver.find_element(by.XPATH, '//*[@id="today_1"]').click()
        time.sleep(5)
        tr = 2
        path = '//*[@id="grid_1"]/tbody/'
        gr = list()
        while True:
            try:
                data = driver.find_element(by.XPATH, path + 'tr[{}]'.format(tr))
                attr = data.get_attribute('class')
                if attr == 'date_separe':
                    gr = str(driver.find_element(by.XPATH, path + 'tr[{}]/td[1]'.format(tr)).text).split(' - ')
                if attr == 'line':
                    res = str(data.text).split('\n')
                    tm = res[2].split(' - ')
                    json_load('Access Bet', 'nigeria', res[1], day[5], tm[0], tm[1], gr[1],
                              gr[0], res[5], res[3], res[4])
                tr += 1
            except Exception as e:
                create_log('Accessbet: nigeria', 'getting_data: {}'.format(e), 'error')
                break

    def site_20(self):
        driver.get('https://www.surebet247.com/sports/football/')
        time.sleep(3)
        driver.find_element(by.XPATH, '//*[@id="tabs-Center_TabSwitchResponsiveBlock_22357"]/li[3]').click()
        time.sleep(5)
        link = '//*[@id="html-container-Center_DailyMatchListResponsiveBlock_22356"]/'
        tr = 1
        while True:
            try:
                if driver.find_element(by.XPATH, link + 'h4[{}]'.format(tr)).get_attribute('class') != \
                        'branchLeagueHeadline toggleableHeadline expanded   ':
                    driver.find_element(by.XPATH, link + 'h4[{}]'.format(tr)).click()

                gr = str(driver.find_element(by.XPATH, link + 'h4[{}]'.format(tr)).text).split('\n')[1].split(' - ')
                div = 1
                while True:
                    try:
                        driver.execute_script("arguments[0].scrollIntoView();",
                                              driver.find_element(by.XPATH, link + 'div[{}]/div[{}]'.format(tr, div)))
                        res = str(driver.find_element(by.XPATH, link + 'div[{}]/'
                                                                       'div[{}]'.format(tr, div)).text).split('\n')
                        if res[0] != 'Today':
                            div += 1
                            continue
                        if len(gr) == 2:
                            json_load('Sure Bet 247', 'nigeria', res[2], day[5], res[1], res[3],
                                      gr[1], gr[0], res[9], res[5], res[7])
                        else:
                            json_load('Sure Bet 247', 'nigeria', res[2], day[5], res[1], res[3],
                                      gr[0], None, res[9], res[5], res[7])
                        div += 1
                    except Exception as e:
                        create_log('Sure Bet: nigeria', 'getting_data: {}'.format(e), 'error')
                        break
                tr += 1
            except Exception as e:
                create_log('Sure Bet: nigeria', 'getting_group: {}'.format(e), 'error')
                break

    def site_21(self):
        driver.get('https://www.betdey.com/sport/1')
        driver.find_element(by.XPATH, '/html/body/div[3]/main/custom/div[1]/div[2]/form/div[9]').click()
        link = '//*[@id="sport-page-form"]/'
        div = 1
        gr = None
        while True:
            try:
                attribute = driver.find_element(by.XPATH, link + 'div[{}]'.format(div)).get_attribute('class')
                if attribute == 'header':
                    gr = driver.find_element(by.XPATH, link + 'div[{}]'.format(div)).text
                    gr = str(gr).split('\n')[0]
                if attribute == 'event-panel':
                    res = str(driver.find_element(by.XPATH, link + 'div[{}]'.format(div)).text).split('\n')
                    tm = res[2].split(' v ')
                    json_load('Bet Dey', 'nigeria', res[0], day[5], tm[0], tm[1], gr, None, res[5], res[3], res[4])
                div += 1
            except Exception as e:
                create_log('Bet Dey: nigeria', 'getting_data: {}'.format(e), 'error')
                break

    def site_22(self):
        from threading import Thread
        driver.get('https://1xbet9ja.com/us/line/')

        def change_table(step: int, _driver, _by):
            try:
                if step == 1:
                    _driver.find_element(_by.XPATH, '//*[@id="countLiveEventsOnMain"]/div[1]').click()
                else:
                    _driver.find_element(_by.XPATH, '//*[@id="countLiveEventsOnMain"]/div[2]/div[7]').click()

                if _driver.find_element(_by.XPATH, '//*[@id="countLiveEventsOnMain"]/div[1]').text != 'TOP 200':
                    change_table(step, _driver, _by)
            except Exception as e:
                create_log('IXBet9Ja: nigeria', 'thread: {}'.format(e), 'error')
                change_table(step, _driver, _by)

        threads = [Thread(target=change_table, args=(i, driver, by)) for i in range(1, 3)]
        [thread.start() for thread in threads]
        [thread.join() for thread in threads]

        time.sleep(3)
        try:
            f_path = '//*[@id="sports_left"]/div/div[1]/div/div[4]/div/div[1]/div/'
            driver.find_element(by.XPATH, '//*[@id="sports_left"]/div/div[1]/div/div[3]/div/button').click()
            driver.find_element(by.XPATH, f_path + 'div/div/span').click()
            driver.find_element(by.XPATH, f_path + 'div[2]/div[1]/div[2]/div[2]/a/span').click()
            driver.find_element(by.XPATH, f_path + 'div[2]/div[1]/div[2]/div[2]/div/ul/li[2]').click()
            driver.find_element(by.XPATH, f_path + 'div[2]/div[2]/div[2]/button[1]').click()
            time.sleep(3)
        except Exception as e:
            create_log('IXBet9Ja: nigeria', 'filter_changing: {}'.format(e), 'error')

        path = '//*[@id="games_content"]/div/div[2]/div/'
        div = 1
        while True:
            try:
                gr = str(driver.find_element(by.XPATH, path + 'div[{}]/div[1]'.format(div)).text).split('\n')
                mat = 2
                while True:
                    try:
                        res = str(driver.find_element(by.XPATH, path + 'div[{}]/'
                                                                       'div[{}]'.format(div, mat)).text).split('\n')
                        if mat == 2:
                            res.pop(0)

                        t_m = res[0].split(' ')
                        tm = str(driver.find_element(by.XPATH, path + 'div[{}]/div[{}]/'
                                                                      'div[2]/a'.format(div, mat)).text).split('\n')
                        odds = str(driver.find_element(by.XPATH, path + 'div[{}]/div[{}]/div[2]/'
                                                                        'div[5]'.format(div, mat)).text).split('\n')
                        json_load('IXBet9Ja', 'nigeria', t_m[1], t_m[0].split('.')[0],
                                  tm[0], tm[1], gr[0], None, odds[2], odds[0], odds[1])
                        mat += 1
                    except Exception as e:
                        create_log('IXBet9Ja: nigeria', 'getting_data: {}'.format(e), 'error')
                        div += 1
                        break
            except Exception as e:
                create_log('IXBet9Ja: nigeria', 'getting_group: {}'.format(e), 'error')
                break


    def site_23(self):
        driver.get('https://www.naija4win.com/odds.asp')
        time.sleep(5)
        link = '/html/body/div[1]/div/div/div[3]/div[2]/div/div[2]/'
        driver.find_element(by.XPATH, link + 'form[1]/ul/li[1]/div/div/a').click()
        time.sleep(1)
        driver.find_element(by.XPATH, link + 'form[1]/ul/li[1]/div/ul/li[2]/a').click()
        driver.find_element(by.XPATH, '//*[@id="bob_Oddslist_Form_Submit"]').click()
        time.sleep(15)
        tr = 1
        link = '/html/body/div[1]/div/div/div[3]/div[2]/div/div[2]/table/tbody/'
        gr = None
        tm = None
        while True:
            try:
                if driver.find_element(by.XPATH, link + 'tr[{}]/td[1]'.format(tr)).get_attribute('class')\
                        == 'Bob_Oddslist_Gamehead':
                    gr = str(driver.find_element(by.XPATH, link + 'tr[{}]'.format(tr)).text)
                    if 'Both teams' in gr:
                        break
                    gr = gr.split('Closing CETSoccer > ')[1].split(' ')
                elif driver.find_element(by.XPATH, link + 'tr[{}]/td[1]'.format(tr)).get_attribute('class')\
                        == 'gametime':
                    tm = str(driver.find_element(by.XPATH, link + 'tr[{}]'.format(tr)).text).split(' ')
                elif driver.find_element(by.XPATH, link + 'tr[{}]/td[1]'.format(tr)).get_attribute('class')\
                        == 'gamename':
                    res = str(driver.find_element(by.XPATH, link + 'tr[{}]'.format(tr)).text).split('\n')
                    if res[0].count('-') == 2:
                        t_m = ''
                        a = list(res[0])
                        dic = [len(a) / 2]
                        for i in range(len(a)):
                            if a[i] == '-':
                                dic.append(i)
                        if m.fabs(dic[0] - dic[1]) > m.fabs(dic[0] - dic[2]):
                            a[dic[2]] = '#'
                        else:
                            a[dic[1]] = '#'
                        t_m = t_m.join(a).split('#')
                    else:
                        t_m = res[0].split('-')
                    if len(tm[0]) == 1:
                        tm[0] = '0' + str(tm[0])
                    json_load('Naija 4 Win', 'nigeria', tm[2], tm[0], t_m[0],
                              t_m[1], gr[1], gr[0], res[3], res[1], res[2])
                tr += 1
            except Exception as e:
                create_log('Naija 4 Win: nigeria', 'getting_data: {}'.format(e), 'error')
                break

    def site_24(self):
        driver.get('https://m-bet.co.zm')
        table = '//*[@id="fixtures-section"]/div[1]/div/'
        run_match, match = True, 2
        exc = 0
        while run_match:
            try:
                elem = driver.find_element(by.XPATH, table + 'div[{}]'.format(match))
                driver.execute_script("arguments[0].scrollIntoView();", elem)
                res = str(elem.text).split('\n')
                gr = str(driver.find_element(by.XPATH, table + 'div[{}]/div[1]/div[2]/'
                                                               'div[1]'.format(match)).text).split(' - ')
                tm = str(driver.find_element(by.XPATH, table + 'div[{}]/div[1]/div[2]/'
                                                               'div[2]'.format(match)).text).split(' VS. ')
                json_load('M-Bet', 'zambia', res[0], day[5], tm[0], tm[1], gr[0], gr[1], res[4], res[2], res[3])
                match += 1
            except Exception as e:
                exc += 1
                run_match = False if exc > 1 else True
                table = '//*[@id="fixtures-section"]/div[3]/div/'
                match = 1
                time.sleep(3)
                create_log('M-Bet: zambia', 'getting_data: {}'.format(e), 'error')

    def site_25(self):
        driver.get('https://www.betpawa.co.zm/upcoming')
        l = '/html/body/div[2]/div/div[2]/div[1]/div[5]/'
        d = 1
        b = 1
        exc = 0
        while True:
            try:
                driver.execute_script("arguments[0].scrollIntoView();",
                                      driver.find_element(by.XPATH,l + 'div[{}]/div[{}]'.format(b, d)))
                tm = str(driver.find_element(by.XPATH, l + 'div[{}]/div[{}]/a/div/h3[2]'.format(b, d)).text).split('\n')
                if 'Pawaboost' in tm[1]:
                    tm[1] = tm[2]
                if tm[1].split('/')[0].split(' ')[1] != day[5]:
                    return
                h = str(driver.find_element(by.XPATH, l + 'div[{}]/div[{}]/a/div/'
                                                          'h3[1]'.format(b, d)).text).replace(' (n)', '').split(' - ')
                gr = str(driver.find_element(by.XPATH, l + 'div[{}]/div[{}]/a/div[2]'.format(b, d)).text).split(' - ')
                odd = str(driver.find_element(by.XPATH, l + 'div[{}]/div[{}]/div'.format(b, d)).text).split('\n')
                if 'am' in tm[0]:
                    tm = tm[0].split(' ')[0]
                else:
                    tm = tm[0].split(' ')[0].split(':')
                    if tm[0] == '12':
                        tm = '00:{}'.format(tm[1])
                    else:
                        tm = str(int(tm[0]) + 12) + ':' + tm[1]
                country = ['kenya', 'tanzania', 'uganda', 'zambia', 'ghana', 'nigeria']
                for i in country:
                    json_load('Bet Pawa', i, tm, day[5], h[0], h[1], gr[0], gr[1],
                              odd[2].split(' ')[1], odd[0].split(' ')[1], odd[1].split(' ')[1])
                d += 1
                exc = 0
            except Exception as e:
                create_log('Bet Pawa: countries', 'getting_data: {}'.format(e), 'error')
                exc += 1
                if exc >= 5:
                    break
                b += 1
                d = 1

    def site_26(self):
        driver.get('https://sports.premierbet.com/en?timestamp=1549230852&removet=1&lang=en')
        time.sleep(2)
        driver.find_element(by.XPATH, '//*[@id="day1"]').click()
        time.sleep(2)
        table = '//*[@id="EventList"]/div/'
        group_run, group = True, 2
        while group_run:
            try:
                gr = str(driver.find_element(by.XPATH, table + 'div[{}]/div[1]/div[1]/div[5]'
                                             .format(group)).text).split(' -> ')
                match_run, match = True, 3
                while match_run:
                    try:
                        res = str(driver.find_element(by.XPATH, table + 'div[{}]/div[2]/div[2]/div[{}]'
                                                      .format(group, match)).text).split('\n')
                        h = driver.find_element(by.XPATH, table + 'div[{}]/div[2]/div[2]/div[{}]/div[2]/a/div/span[1]'
                                                .format(group, match)).text
                        a = driver.find_element(by.XPATH, table + 'div[{}]/div[2]/div[2]/div[{}]/div[2]/a/div/span[2]'
                                                .format(group, match)).text
                        json_load('Premier Bet', 'zambia', res[0], day[5], h, a, gr[1], gr[0], res[5], res[3], res[4])
                        match += 1
                    except Exception as e:
                        create_log('Premier Bet: countries', 'getting_data: {}'.format(e), 'error')
                        match_run = False
                        group += 1
            except Exception as e:
                create_log('Premier Bet: countries', 'getting_group: {}'.format(e), 'error')
                group_run = False

    def site_27(self):
        driver.get('http://gsb.co.zm/sports')
        time.sleep(3)
        driver.find_element(by.XPATH, '//*[@id="yui-gen7"]/div[2]/a[3]').click()
        time.sleep(2)
        driver.find_element(by.XPATH, '/html/body/div[4]/div[2]/div[1]/div[2]/div[2]/ul[1]/li/div/input').click()
        time.sleep(5)
        div, mat = 1, 1
        exc = 0
        path = '//*[@id="yui-gen3"]/div[{}]/div[1]/div[2]/div/div/div[2]/div[{}]'
        while exc < 15:
            try:
                res = str(driver.find_element(by.XPATH, path.format(div, mat)).text).split('\n')
                res.remove('LIVE') if 'LIVE' in res else None
                date = res[1].split(' ')[0]
                tm = res[3].split(' v ')
                json_load('GSB Zambia', 'zambia', res[0], date, tm[0], tm[1], res[5], res[4], res[8], res[7], res[9])
                mat += 1
                exc = 0
            except Exception as e:
                create_log('GSB Zambia: zambia', 'getting_data: {}'.format(e), 'error')
                exc += 1
                div += 1
                mat = 1

    def site_28(self):
        driver.get('https://sports.sportingbet.co.za/en/sports#sportId=4')
        time.sleep(5)
        path = '//*[@id="markets"]/div/div[2]/div/div/div/div/'
        div = 1
        while True:
            try:
                gr = str(driver.find_element(by.XPATH, path + 'div[{}]/h2/span'.format(div)).text).split(' - ')
                mat = 1
                while True:
                    try:
                        res = str(driver.find_element(by.XPATH, path + 'div[{}]/div/div[{}]'.format(div, mat)).text)
                        res = res.split('\n')
                        json_load('Sporting Bet', 'south africa', res[0], day[5], res[2],
                                  res[6], gr[0], gr[1], res[7], res[3], res[5])
                        json_load('Sporting Bet', 'zambia', res[0], day[5], res[2],
                                  res[6], gr[0], gr[1], res[7], res[3], res[5])
                        mat += 1
                    except Exception as e:
                        create_log('Sporting Bet: south africa', 'getting_data: {}'.format(e), 'error')
                        div += 1
                        break
            except Exception as e:
                create_log('Sporting Bet: south africa', 'getting_group: {}'.format(e), 'error')
                break

    def site_29(self):
        driver.get('https://www.interbet.co.za/SoccerZone.aspx/soccer-betting-odds/'
                   'starting-in-4-hours/?sport=soccer&middlenav=48_Soccer_60__1')
        time.sleep(5)
        link = '//*[@id="MainEventContainer"]/div[6]/div[2]/'
        tl = 1

        while True:
            try:
                gr = driver.find_element(by.XPATH, link + 'table[{}]/thead/tr[1]/td[1]'.format(tl)).text
                gr = str(gr).strip(')').split(' (')
                tr = 1
                while True:
                    try:
                        odd = []
                        for i in range(3, 6):
                            odd.append(driver.find_element(by.XPATH, link + 'table[{}]/tbody/tr[{}]/td[{}]/div/input'
                                                           .format(tl, tr, i)).get_attribute('value'))
                        res = str(driver.find_element(by.XPATH, link + 'table[{}]/tbody/tr[{}]'
                                                      .format(tl, tr)).text).split('\n')
                        tm = res[0].split(' ')
                        t_m = res[1].split(' v ')
                        json_load('Interbet', 'south africa', tm[2], tm[0], t_m[0],
                                  t_m[1], gr[0], gr[1], odd[2], odd[0], odd[1])
                        json_load('Interbet', 'zambia', tm[2], tm[0], t_m[0],
                                  t_m[1], gr[0], gr[1], odd[2], odd[0], odd[1])
                        tr += 1
                    except Exception as e:
                        create_log('Interbet: south africa', 'getting_data: {}'.format(e), 'error')
                        tl += 1
                        break
            except Exception as e:
                create_log('Interbet: south africa', 'getting_group: {}'.format(e), 'error')
                break

    def site_30(self):
        driver.get('https://www.worldsportsbetting.co.za/#!/coupon/1436/{}-{}-{}'.format(day[1], day[3], day[5]))
        path = '//*[@id="html_content"]/div/div/div/div[{}]'
        div = 1
        gr = str()
        while True:
            try:
                elem = driver.find_element(by.XPATH, path.format(div))
                if elem.get_attribute('class') == 'alt_title_new blinds':
                    gr = elem.text
                    div += 1
                    continue
                mat = 1
                while True:
                    try:
                        elem = driver.find_element(by.XPATH, path.format(div) + '/div[{}]'.format(mat))
                        if elem.get_attribute('class') == 'bcont':
                            res = str(elem.text).split('\n')
                            t_m = str(elem.find_element(by.XPATH, 'div[1]/div').text)
                            tm = res[0].replace(t_m, '').split(' vs ')
                            t_m = t_m.split(' ')
                            odds = res[1].split(' ')[:3]
                            odds = [odds[0].split('/'), odds[1].split('/'), odds[2].split('/')]
                            odds[0] = float("%.2f" % (int(odds[0][0]) / int(odds[0][1]) + 1))
                            odds[1] = float("%.2f" % (int(odds[1][0]) / int(odds[1][1]) + 1))
                            odds[2] = float("%.2f" % (int(odds[2][0]) / int(odds[2][1]) + 1))
                            json_load('WorldSportsbetting', 'zambia', t_m[2], t_m[0], tm[0], tm[1], gr,
                                      None, odds[2], odds[0], odds[1])
                        mat += 1
                    except Exception as e:
                        create_log('WorldSportsbetting: zambia', 'getting_data: {}'.format(e), 'error')
                        div += 1
                        break
            except Exception as e:
                create_log('WorldSportsbetting: zambia', 'getting_group: {}'.format(e), 'error')
                break


    def site_31(self):
        driver.get('https://www.scorebet.co.za/soccer/')
        path = '//*[@id="coupon-page"]/div[2]/div/div[1]/div[2]/div[{}]'
        run_match, match = True, 2
        while run_match:
            try:
                elem = driver.find_element(by.XPATH, path.format(match))
                driver.execute_script("arguments[0].scrollIntoView();", elem)
                if elem.get_attribute('class') == 'accordion':
                    break
                res = str(elem.text).split('\n')
                tm = res[0].split(' v ')
                t_m = res[1].replace(',', '').split(' ')
                json_load('Scorebet', 'zambia', t_m[5], t_m[2], tm[0], tm[1], None, None, res[4], res[2], res[3])
                match += 1
            except Exception as e:
                run_match = False
                create_log('Scorebet: zambia', 'getting_data: {}'.format(e), 'error')

    def site_32(self):
        driver.get('https://www.wsbetting.com/#/sports/Sport_Football/TODAY')
        time.sleep(9)
        table = '//*[@id="ALL_PREMATCH_SINGLE_Sport_Football"]/'
        run_country, coun = True, 2
        while run_country:
            try:
                gr = driver.find_element(by.XPATH, table + 'div[{}]/div[1]/span[1]'.format(coun)).text
                lg = driver.find_element(by.XPATH, table + 'div[{}]/div[1]/span[2]'.format(coun)).text
                run_match, match = True, 2
                while run_match:
                    try:
                        res = driver.find_element(by.XPATH, table + 'div[{}]/div[{}]'.format(coun, match)).text
                        res = str(res).split('\n')
                        t_m = res[0].split(' ')
                        json_load('World Star Betting', 'uganda', t_m[1],
                                  day[5], res[1], res[2], lg, gr, res[5], res[3], res[4])
                        match += 1
                    except Exception as e:
                        create_log('World Star Betting: Uganda', 'getting_data: {}'.format(e), 'error')
                        run_match = False
                        coun += 1
            except Exception as e:
                create_log('World Star Betting: Uganda', 'getting_group: {}'.format(e), 'error')
                run_country = False

    def site_33(self):
        driver.get('https://u-bet.co.ug/')
        table = '//*[@id="fixtures-section"]/div[1]/div/'
        run_match, match = True, 2
        exc = 0
        while run_match:
            try:
                elem = driver.find_element(by.XPATH, table + 'div[{}]'.format(match))
                driver.execute_script("arguments[0].scrollIntoView();", elem)
                res = str(elem.text).split('\n')
                gr = str(driver.find_element(by.XPATH, table + 'div[{}]/div[1]/div[2]/'
                                                               'div[1]'.format(match)).text).split(' - ')
                tm = str(driver.find_element(by.XPATH, table + 'div[{}]/div[1]/div[2]/'
                                                               'div[2]'.format(match)).text).split(' VS. ')
                json_load('U-Bet', 'uganda', res[0], day[5], tm[0], tm[1], gr[0], gr[1], res[4], res[2], res[3])
                match += 1
            except Exception as e:
                exc += 1
                run_match = False if exc > 1 else True
                table = '//*[@id="fixtures-section"]/div[3]/div/'
                match = 1
                time.sleep(6)
                create_log('U-Bet: uganda', 'getting_data: {}'.format(e), 'error')

    def site_34(self):
        driver.get('https://sports.premierbet.com/en?timestamp=1531687564&removet=1&lang=en')
        time.sleep(3)
        driver.find_element(by.XPATH, '//*[@id="day1"]').click()
        time.sleep(5)
        link = '/html/body/div/div/div/div/div[10]/div/div/div[4]/div/div/div/'
        div = 2
        while True:
            try:
                gr = str(driver.find_element(by.XPATH, link + 'div[{}]/div[1]/div[1]/div[5]'.format(div)).text).split(' -> ')
                match = 3
                while True:
                    try:
                        res = str(driver.find_element(by.XPATH, link + 'div[{}]/div[2]/div[2]/'
                                                                       'div[{}]'.format(div, match)).text).split('\n')
                        h = str(driver.find_element(by.XPATH, link + 'div[{}]/div[2]/div[2]/div[{}]/'
                                                                     'div[2]/a/div/span[1]'.format(div, match)).text)
                        a = str(driver.find_element(by.XPATH, link + 'div[{}]/div[2]/div[2]/div[{}]/'
                                                                     'div[2]/a/div/span[2]'.format(div, match)).text)
                        match += 1
                        json_load('Sports Betting Africa (SBA)', 'uganda', res[0], day[5],
                                  h, a, gr[1], gr[0], res[5], res[3], res[4])
                    except Exception as e:
                        create_log('Sports Betting Africa: uganda', 'getting_data: {}'.format(e), 'error')
                        break
                div += 1
            except Exception as e:
                create_log('Sports Betting Africa: uganda', 'getting_group: {}'.format(e), 'error')
                break

    def site_35(self):
        driver.get('https://www.beton.co.ug/sportsbet/today-events/1')
        time.sleep(5)
        path = '//*[@id="today-events-container"]/'
        div = 1
        try:
            driver.find_element(by.XPATH, '//*[@id="modalOnEnter"]/div/button').click()
            driver.find_element(by.XPATH, '//*[@id="fixtures_page"]/div/div[2]/div/div[1]/ul/li[1]/a').click()
            time.sleep(3)
        except Exception as e:
            create_log('Betin: uganda', 'update_view: {}'.format(e), 'error')
            pass
        while True:
            try:
                gr = str(driver.find_element(by.XPATH, path + 'div[{}]/div[1]/div[1]/'
                                                              'h5'.format(div)).text).split('\n')[0]
                lg = str(driver.find_element(by.XPATH, path + 'div[{}]/div[1]/div[2]/'
                                                              'div[1]/div[1]'.format(div)).text).split('\n')[0]

                mat = 2
                while True:
                    try:
                        elem = driver.find_element(by.XPATH, path + 'div[{}]/div[1]/div[2]/div[1]/div[2]/'
                                                                    'div[2]/div[{}]'.format(div, mat))
                        driver.execute_script("arguments[0].scrollIntoView(true);", elem)
                        res = str(elem.text).split('\n')
                        res.remove('L') if 'L' in res else None
                        date = res[1].split('/')[0]
                        json_load('Betin', 'uganda', res[0], date, res[2], res[3], lg, gr, res[9], res[5], res[7])
                        mat += 1
                    except Exception as e:
                        create_log('Betin: uganda', 'getting_data: {}'.format(e), 'error')
                        div += 1
                        break
            except Exception as e:
                create_log('Betin: uganda', 'getting_group: {}'.format(e), 'error')
                break

    def site_36(self):
        driver.get('https://www.goal.ug/#/sport/?type=0')
        driver.maximize_window()
        time.sleep(8)
        link = '/html/body/div[1]/div[2]/div/div/div/div/div/div[1]/div/div[1]/div[2]/div[1]/ul/'
        driver.find_element(by.XPATH, link + 'li[2]/i').click()
        driver.find_element(by.XPATH, link + 'li[1]/div/p/span').click()
        time.sleep(1)
        driver.find_element(by.XPATH, link + 'li[1]/div/div/ul/li[7]/span').click()
        time.sleep(5)
        for i in range(2):
            driver.find_element(by.XPATH, '/html/body/div[1]/div[2]/div/div/div/div/div/div[1]/div/div[2]/div[2]/'
                                          'include-template[3]/div/ul/li[3]/ul/li[2]/div[1]').click()
        link = '/html/body/div[1]/div[2]/div/div/div/div/div/div[1]/div/div[2]/' \
               'div[2]/include-template[3]/div/ul/li[3]/ul/li[2]/div[2]/div/'
        time.sleep(2)
        div = 1
        while True:
            try:
                driver.find_element(by.XPATH, link + 'div[{}]/div[1]'.format(div)).click()
                time.sleep(1.5)
                mat = 1
                while True:
                    try:
                        driver.find_element(by.XPATH, link + 'div[{}]/div[2]/div[1]/'
                                                             'div[1]/div[{}]'.format(div, mat)).click()
                        mat += 1
                    except:
                        div += 1
                        break
            except:
                break
        div = 2
        link = '/html/body/div[1]/div[2]/div/div/div/div/div/div[2]/div/div[1]/div[2]/include-template/div/'
        while True:
            try:
                gr = driver.find_element(by.XPATH, link + 'div[{}]/div[1]'.format(div)).text
                gr = str(gr).split('FOOTBALL. ')[1].split(' (POSSIBLE FORMAT CHANGE)')[0].split(' - ')
                tm = driver.find_element(by.XPATH, link + 'div[{}]/div[2]/div[1]'.format(div)).text
                tm = str(tm).split('.')[0]
                tl = 1
                while True:
                    try:
                        res = str(driver.find_element(by.XPATH, link + 'div[{}]/div[2]/'
                                                                       'table[{}]'.format(div, tl)).text).split('\n')
                        if ('+' or '-') in res[6] or ('+' or '-') in res[5] or ('+' or '-') in res[4]:
                            tl += 1
                            continue
                        json_load('Goal Sports Betting', 'uganda', res[1], tm, res[0],
                                  res[3], gr[1], gr[0], res[6], res[4], res[5])
                        tl += 1
                    except:
                        div += 1
                        break
            except:
                break

    def site_37(self):
        driver.get('https://www.pstbet.com/odds.asp')
        time.sleep(4)
        driver.find_element(by.XPATH, '//*[@id="formas"]/ul/li[1]/div/div/a').click()
        time.sleep(1)
        driver.find_element(by.XPATH, '//*[@id="formas"]/ul/li[1]/div/ul/li[2]/a').click()
        time.sleep(1)
        driver.find_element(by.XPATH, '//*[@id="formas"]/ul/li[3]/div/div/a').click()
        time.sleep(1)
        driver.find_element(by.XPATH, '//*[@id="formas"]/ul/li[3]/div/ul/li[1]/a').click()
        driver.find_element(by.XPATH, '//*[@id="bob_Oddslist_Form_Submit"]').click()
        time.sleep(15)
        link = '/html/body/div[2]/div/div/div[3]/div[2]/div/div[2]/table/'
        tr = 1
        dt = None
        gr = None
        while True:
            try:
                elem = driver.find_element(by.XPATH, link + 'tbody/tr[{}]'.format(tr))
                if elem.get_attribute('class') == 'games_date':
                    if driver.find_element(by.XPATH, link + 'tbody/tr[{}]'.format(tr - 1)).get_attribute('class') == '':
                        gr = driver.find_element(by.XPATH, link + 'tbody/tr[{}]'.format(tr - 1)).text
                        gr = str(gr).split(' > ')[1]
                        if 'Both' in gr or 'Double' in gr or 'Draw' in gr or 'Half' in gr:
                            break
                        gr = gr.split(' ', 1)
                    dt = str(elem.text).split(' ')
                if elem.get_attribute('class') == 'games':
                    match = str(elem.text).split('\n')
                    tm = match[0].split('-')
                    if len(dt[0]) == 1:
                        dt[0] = '0' + str(dt[0])
                    json_load('Premier Sports Trading', 'namibia', dt[2],
                              dt[0], tm[0], tm[1], gr[1], gr[0], match[3], match[1], match[2])
                tr += 1
            except Exception as e:
                create_log('Premier Sports Trading: namibia', 'getting_data: {}'.format(e), 'error')
                break

    def site_38(self):
        driver.get('https://sports.premierbet.com/en?timestamp=1530694024&removet=1&lang=en')
        time.sleep(3)
        driver.find_element(by.XPATH, '//*[@id="day1"]').click()
        time.sleep(10)
        link = '/html/body/div/div/div/div/div[10]/div/div/div[4]/div/div/div/'
        div_g = 2
        while True:
            try:
                gr = str(driver.find_element(by.XPATH, link + 'div[{}]/div[1]/div[1]/'
                                                              'div[5]'.format(div_g)).text).split(' -> ')
                div = 3
                while True:
                    try:
                        res = str(driver.find_element(by.XPATH, link + 'div[{}]/div[2]/div[2]/'
                                                                       'div[{}]'.format(div_g, div)).text).split('\n')
                        ht = driver.find_element(by.XPATH, link + 'div[{}]/div[2]/div[2]/div[{}]/div[2]/'
                                                                  'a/div/span[1]'.format(div_g, div)).text
                        at = driver.find_element(by.XPATH, link + 'div[{}]/div[2]/div[2]/div[{}]/div[2]/'
                                                                  'a/div/span[2]'.format(div_g, div)).text
                        bets = []
                        for i in range(3, 6):
                            buf = driver.find_element(by.XPATH, link + 'div[{}]/div[2]/div[2]/div[{}]'
                                                                       '/div[{}]'.format(div_g, div, i)).text
                            if buf == '':
                                bets.append(None)
                            else:
                                bets.append(buf)
                        json_load('Premier Bet', 'malawi', res[0], day[5], ht, at,
                                  gr[1], gr[0], bets[2], bets[0], bets[1])
                        div += 1
                    except Exception as e:
                        create_log('Premier Bet: malawi', 'getting_data: {}'.format(e), 'error')
                        div_g += 1
                        break
            except Exception as e:
                create_log('Premier Bet: malawi', 'getting_group: {}'.format(e), 'error')
                break

    def site_39(self):
        driver.get('https://www.mkekabet.com/sportsbook/SOCCER/today1x2coupon/')
        time.sleep(3)
        div = 1
        link = '//*[@id="947044713"]/div/'
        while True:
            try:
                gr = driver.find_element(by.XPATH, link + 'div[{}]/div[1]/span[2]'.format(div)).text
                gr = str(gr).split(' ', 1)
                mat = 1
                while True:
                    try:
                        res = driver.find_element(by.XPATH, link + 'div[{}]/div[3]/div[{}]'.format(div, mat)).text
                        res = str(res).split('\n')
                        json_load('Mkekabet', 'tanzania', res[0], day[5], res[2],
                                  res[3], gr[1], gr[0], res[6], res[4], res[5])
                        mat += 1
                    except Exception as e:
                        create_log('Mkekabet: tanzania', 'getting_data: {}'.format(e), 'error')
                        div += 1
                        break
            except Exception as e:
                create_log('Mkekabet: tanzania', 'getting_group: {}'.format(e), 'error')
                break

    def site_40(self):
        driver.get('https://wakabet.co.tz')
        link = '/html/body/div[1]/div/div/div[2]/div/div/table/tbody/'
        tr = 2
        exc = 0
        while exc < 15:
            try:
                gr = driver.find_element(by.XPATH, link + 'tr[{}]/td[2]/div[1]/'
                                                          'small/a/img'.format(tr)).get_attribute('alt')
                bet = []
                for i in range(1, 4):
                    bet.append(driver.find_element(by.XPATH, link + 'tr[{}]/td[2]/div[2]/'
                                                                    'div/table/tbody/tr/td[{}]'.format(tr, i)).text)
                elem = driver.find_element(by.XPATH, link + 'tr[{}]'.format(tr))
                driver.execute_script("arguments[0].scrollIntoView(true);", elem)
                res = str(elem.text).split('\n')
                t_m = res[2].split(' vs ')
                lg = res[1].split('- ')[1]
                if '45 Minutes 2nd' not in res:
                    json_load('Wakabet', 'tanzania', res[0], day[5], t_m[0], t_m[1], lg, gr, bet[2], bet[0], bet[1])
                tr += 1
                exc = 0
            except Exception as e:
                create_log('Wakabet: tanzania', 'getting_data: {}'.format(e), 'error')
                exc += 1
                tr += 1
                time.sleep(1)

    def site_41(self):
        driver.get('https://sportpesa.co.tz/?sportId=1&section=today')
        li = ['//*[@id="mainview"]/div/div/div[1]/div[2]/div[3]/events-content/div[4]/events-list/basic-', 3]
        time.sleep(5)
        link = '//*[@id="mainview"]/div/div/div[1]/div[2]/div[3]/events-content/div[4]/events-list/section/div[2]/'
        gr_atr = 'ng-animate-enabled ng-app-slide-animation event-set-info-row-container ng-scope'
        div = 2
        gr = None
        while True:
            try:
                if driver.find_element(by.XPATH, link + 'div[{}]/div[1]'.format(div)).get_attribute('class') == gr_atr:
                    gr = driver.find_element(by.XPATH, link + 'div[{}]/div[1]/div[1]/div[1]'.format(div)).text
                    gr = str(gr).split('(')[0].split(' - ')
                    res = str(driver.find_element(by.XPATH, link + 'div[{}]/div[2]'.format(div)).text).split('\n')
                else:
                    res = str(driver.find_element(by.XPATH, link + 'div[{}]/div[1]'.format(div)).text).split('\n')
                div += 1
                json_load('Sportpesa', 'tanzania', res[0], day[5], res[3], res[4], gr[1], gr[0], res[7], res[5], res[6])
            except Exception as e:
                create_log('Sportpesa: tanzania', 'getting_data: {}'.format(e), 'error')
                if driver.find_element(by.XPATH, li[0] + 'pagination/ul/li[{}]'
                                       .format(li[1])).get_attribute('class') != 'ng-scope page-disabled':
                    driver.find_element(by.XPATH, li[0] + 'pagination/ul/li[{}]'.format(li[1])).click()
                    li[1] += 1
                    time.sleep(3)
                    div = 2
                else:
                    break

    def site_42(self):
        driver.get('https://betyetu.co.ke/sportsbook/SOCCER/')
        table = '/html/body/main/div/div/div[2]/section[3]/div/div[2]/div/div/div/'
        run_country, country = True, 1
        while run_country:
            try:
                lg = str(driver.find_element(by.XPATH, table + 'div[{}]/div[1]'.format(country)).text).split('\n')[0]
                run_match, match = True, 1
                while run_match:
                    try:
                        res = str(driver.find_element(by.XPATH, table + 'div[{}]/div[3]/div[{}]'.format(country, match)).text).split('\n')
                        tm = res[2].split(' v ')
                        json_load('Betyetu', 'kenya', res[0], day[5], tm[0], tm[1], lg, None, res[5], res[3], res[4])
                        match += 1
                    except Exception as e:
                        create_log('Betyetu: kenya', 'getting_data: {}'.format(e), 'error')
                        run_match = False
                        country += 1
            except Exception as e:
                create_log('Betyetu: kenya', 'getting_group: {}'.format(e), 'error')
                run_country = False

    def site_43(self):
        click = 0
        driver.get('http://eazibet.co.ke/Sport/default.aspx')
        time.sleep(2)
        update = '/html/body/form/div[5]/div[1]/div[2]/'
        driver.find_element(by.XPATH, update + 'div[2]/div/div[2]/ul/li[1]').click()
        time.sleep(2)
        run_country, c_iter = True, 1
        while run_country:
            try:
                elem = driver.find_element(by.XPATH, update + 'div[2]/div/div[2]/ul/li[1]/ul[{}]'.format(c_iter))
                elem.click()
                driver.execute_script("arguments[0].scrollIntoView(true);", elem)


                group = str(driver.find_element(by.XPATH, update + 'div[2]/div/div[2]/ul/li[1]/ul[{}]'
                                                .format(c_iter)).text).split('\n')[0]
                run_league, l_iter = True, 1
                while run_league:
                    try:
                        driver.find_element(by.XPATH, update + 'div[2]/div/div[2]/ul/li[1]/ul[{}]/li/ul/li[{}]'
                                                               .format(c_iter, l_iter)).click()
                        time.sleep(1)
                        if click == 0:
                            time.sleep(4)
                            click += 1
                        league = driver.find_element(by.XPATH, update + 'div[2]/div/div[2]/ul/li[1]/ul[{}]/li/ul/li[{}]'
                                                               .format(c_iter, l_iter)).text
                        run_match, match_iter = True, 1
                        while run_match:
                            try:

                                res = str(driver.find_element(by.XPATH, '/html/body/form/div[5]/div[2]/'
                                                                        'span/div[3]/div/div[2]/div[{}]'
                                                              .format(match_iter)).text).split('\n')
                                if len(res) == 1:
                                    if str(res[0].split(' ')[0]) != str(day[5]):
                                        break
                                    else:
                                        match_iter += 1
                                        continue
                                h_t = driver.find_element(by.XPATH, '/html/body/form/div[5]/div[2]/span/div[3]/div/div['
                                                                    '2]/div[{}]/div[4]/span[1]'.format(match_iter)).text
                                a_t = driver.find_element(by.XPATH, '/html/body/form/div[5]/div[2]/span/div[3]/div/div['
                                                                    '2]/div[{}]/div[4]/span[2]'.format(match_iter)).text
                                json_load('eazibet', 'kenya', res[0], day[5], h_t, a_t, league, group,
                                          res[9], res[5], res[7])
                                match_iter += 1
                            except:
                                run_match = False
                        driver.find_element(by.XPATH, update + 'div[2]/div/div[2]/ul/li[1]/ul[{}]/li/ul/li[{}]'
                                            .format(c_iter, l_iter)).click()
                        l_iter += 1
                    except:
                        run_league = False
                        c_iter += 1
                driver.find_element(by.XPATH, update + 'div[2]/div/div[2]/ul/li[1]/ul[{}]'.format(c_iter)).click()
            except:
                run_country = False

    def site_44(self):
        driver.get('https://www.citybet.co.ke/sports')
        time.sleep(5)
        driver.find_element(by.XPATH, '//*[@id="ember47"]/div/div[1]/div').click()
        time.sleep(10)
        path = '//*[@id="ember51"]/div/div[2]/'
        run_country, country = True, 1
        while run_country:
            try:
                gr = str(driver.find_element(by.XPATH, path + 'div[{}]/div[1]/div[1]'.format(country)).text)\
                    .replace(' (Possible Format Change)', '')
                run_match, match = True, 2
                while run_match:
                    try:
                        res = str(driver.find_element(by.XPATH, path + 'div[{}]/div/div[2]/div/div/div[{}]'
                                                                       ''.format(country, match)).text).split('\n')
                        if len(res) > 5:
                            json_load('Citybet', 'kenya', res[2], day[5], res[0],
                                      res[1], gr, None, res[9], res[5], res[7])
                        match += 1
                    except Exception as e:
                        create_log('Citybet: kenya', 'getting_data: {}'.format(e), 'error')
                        run_match = False
                        country += 1
            except Exception as e:
                create_log('Citybet: kenya', 'getting_group: {}'.format(e), 'error')
                run_country = False

    def site_45(self):
        driver.get('https://www.betika.com/')
        table = '/html/body/div[1]/span/main/div[1]/div[2]/div[3]/'
        run_match, match = True, 1
        while run_match:
            try:
                elem = driver.find_element(by.XPATH, table + 'div[{}]'.format(match))
                driver.execute_script("arguments[0].scrollIntoView(true);", elem)
                res = str(elem.text).split('\n')
                gr = res[0].split(' - ')
                t_m = res[1].split(', ')[1]
                json_load('Betika', 'kenya', t_m, day[5], res[2], res[6], gr[2], gr[1], res[7], res[3], res[5])
                match += 1
            except Exception as e:
                create_log('Betika: kenya', 'getting_data: {}'.format(e), 'error')
                run_match = False

    def site_46(self):
        driver.get('https://www.sportybet.com/ke/sport/football/today')
        time.sleep(5)
        table = '//*[@id="importMatch"]/'
        run_country, country, page = True, 2, 3
        while run_country:
            try:
                gr = str(driver.find_element(by.XPATH, table + 'div[{}]/div[1]/div[1]'.format(country)).text).replace(' ', '')
                run_match, match = True, 2
                while run_match:
                    try:
                        elem = driver.find_element(by.XPATH, table + 'div[{}]/div[1]/'
                                                                     'div[3]/div[{}]'.format(country, match))
                        driver.execute_script("arguments[0].scrollIntoView(true);", elem)
                        res = str(elem.text).split('\n')
                        if len(res) > 8:
                            json_load('Sportybet', 'kenya', res[0], day[5], res[2],
                                      res[3], gr, None, res[6], res[4], res[5])
                        match += 1
                    except Exception as e:
                        create_log('Sportybet: kenya', 'getting_data: {}'.format(e), 'error')
                        run_match = False
                        country += 1
            except Exception as e:
                create_log('Sportybet: kenya', 'getting_group: {}'.format(e), 'error')
                elem = driver.find_element(by.XPATH, '//*[@id="importMatch"]/div[{}]/span[{}]'.format(country, page))
                if elem.get_attribute('class') != 'pageNum':
                    run_country = False
                elem.click()
                time.sleep(5)
                country = 2
                page += 1


    def site_47(self):
        driver.get('https://www.mcheza.co.ke/#/sport/?type=0&competition=566&sport=1&region=20001&game=12081829')
        time.sleep(8)
        link = '/html/body/div[1]/div[2]/div/div/div/div/div/div[1]/div/div[1]/div[2]/div[1]/ul/'
        driver.find_element(by.XPATH, link + 'li[2]/i').click()
        driver.find_element(by.XPATH, link + 'li[1]/div/p/span').click()
        driver.find_element(by.XPATH, link + 'li[1]/div/div/ul/li[7]/span').click()
        time.sleep(5)
        for i in range(2):
            driver.find_element(by.XPATH, '/html/body/div/div[2]/div/div/div/div/div/div[1]/div/div[2]/div[2]/'
                                          'include-template[3]/div/ul/li[4]/ul/li[2]/div[1]').click()
        link = '/html/body/div/div[2]/div/div/div/div/div/div[1]/div/div[2]/div[2]/' \
               'include-template[3]/div/ul/li[4]/ul/li[2]/div[2]/div/'
        time.sleep(2)
        div = 1
        while True:
            try:
                driver.find_element(by.XPATH, link + 'div[{}]/div[1]'.format(div)).click()
                driver.find_element(by.XPATH, link + 'div[{}]/div[1]'.format(div)).location_once_scrolled_into_view
                time.sleep(1.5)
                mat = 1
                while True:
                    try:
                        driver.find_element(by.XPATH, link + 'div[{}]/div[2]/div[1]/'
                                                             'div[1]/div[{}]'.format(div, mat)).click()
                        mat += 1
                    except Exception as e:
                        create_log('Mcheza: kenya', 'getting_league_data: {}'.format(e), 'error')
                        div += 1
                        break
            except Exception as e:
                create_log('Mcheza: kenya', 'getting_group_data: {}'.format(e), 'error')
                break
        div = 2
        link = '/html/body/div/div[2]/div/div/div/div/div/div[2]/div/div[1]/div[2]/'
        while True:
            try:
                gr = driver.find_element(by.XPATH, link + 'div[{}]/div[1]'.format(div)).text
                gr = str(gr).split('FOOTBALL. ')[1].split(' (POSSIBLE FORMAT CHANGE)')[0].split(' - ')
                dy = driver.find_element(by.XPATH, link + 'div[{}]/div[2]/div[1]'.format(div)).text
                dy = str(dy).split('.')[0]
                tl = 2
                while True:
                    try:
                        res = str(driver.find_element(by.XPATH, link + 'div[{}]/div[2]/'
                                                                       'div[{}]'.format(div, tl)).text).split('\n')

                        if len(res) != 7:
                            tl += 1
                            continue
                        t_m = res[1].split(' vs ')
                        json_load('Mcheza', 'kenya', res[0], dy, t_m[0], t_m[1], gr[1], gr[0], res[6], res[4], res[5])

                        tl += 1
                    except Exception as e:
                        create_log('Mcheza: kenya', 'getting_data: {}'.format(e), 'error')
                        div += 1
                        break
            except Exception as e:
                create_log('Mcheza: kenya', 'getting_group: {}'.format(e), 'error')
                break

    def site_48(self):
        driver.get('https://betboss.co.ke/sportsbook/SOCCER/')
        time.sleep(3)
        table = '/html/body/main/div/div/div[2]/section[5]/div/div[2]/div/div/div/div[1]/div[3]/'
        run_match, match = True, 1
        while run_match:
            try:
                res = str(driver.find_element(by.XPATH, table + 'div[{}]'.format(match)).text).split('\n')
                tm = res[2].split(' v ')
                json_load('Betboss', 'kenya', res[0], day[5], tm[0], tm[1], None, None, res[5], res[3], res[4])
                match += 1
            except Exception as e:
                run_match = False

    def site_49(self):
        driver.get('https://sportpesa.co.ke/?sportId=1&section=today')
        li = ['//*[@id="mainview"]/div/div/div[1]/div[2]/div[3]/events-content/div[4]/events-list/basic-', 3]

        try:
            driver.find_element(by.XPATH, '//*[@id="homepage-slider"]/div/a[1]').click()
            time.sleep(5)
            driver.find_element(by.XPATH, '//*[@id="mainview"]/div/div/div[1]/sports-menu/nav/'
                                          'ul/li[2]/div[2]/ul/li[5]/div').click()
            time.sleep(3)
        except Exception as e:
            create_log('Sportpesa: kenya', 'getting_main_page: {}'.format(e), 'error')

        link = '//*[@id="mainview"]/div/div/div[1]/div[2]/div[3]/events-content/div[4]/events-list/section/div[2]/'
        gr_atr = 'ng-animate-enabled ng-app-slide-animation event-set-info-row-container ng-scope'
        div = 2
        gr = None
        while True:
            try:
                if driver.find_element(by.XPATH, link + 'div[{}]/div[1]'.format(div)).get_attribute('class') == gr_atr:
                    gr = driver.find_element(by.XPATH, link + 'div[{}]/div[1]/div[1]/div[1]'.format(div)).text
                    gr = str(gr).split('(')[0].split(' - ')
                    res = str(driver.find_element(by.XPATH, link + 'div[{}]/div[2]'.format(div)).text).split('\n')
                else:
                    res = str(driver.find_element(by.XPATH, link + 'div[{}]/div[1]'.format(div)).text).split('\n')
                div += 1
                res[0] = str(res[0])
                json_load('Sportpesa', 'kenya', res[0], day[5], res[3], res[4], gr[1], gr[0], res[7], res[5], res[6])
            except Exception as e:
                create_log('Sportpesa: kenya', 'getting_data: {}'.format(e), 'error')
                if driver.find_element(by.XPATH, li[0] + 'pagination/ul/li[{}]'
                                       .format(li[1])).get_attribute('class') != 'ng-scope page-disabled':
                    driver.find_element(by.XPATH, li[0] + 'pagination/ul/li[{}]'.format(li[1])).click()
                    li[1] += 1
                    time.sleep(3)
                    div = 2
                else:
                    break

    def site_50(self):
        driver.get('https://www.safaribet.co.ke/#/')
        time.sleep(5)
        run_markets, a = True, 1
        link = '/html/body/div[4]/div[2]/section[1]/main/section[1]/section/div/div/div/div/div/div/div/div/div[2]/'
        while run_markets:
            try:
                driver.find_element(by.XPATH, link + 'a[{}]'.format(a)).location_once_scrolled_into_view
                driver.find_element(by.XPATH, link + 'a[{}]'.format(a)).click()
                time.sleep(2)
                a += 1 if a != 2 else 0
            except:
                run_markets = False
        table = '/html/body/div[4]/div[2]/section[1]/main/section[1]/section/div/div/div/div/div/div/div/div/'
        run_match, match = True, 1
        while run_match:
            try:
                res = str(driver.find_element(by.XPATH, table + 'article[{}]'.format(match)).text).split('\n')
                tm = res[4].split(' v ')
                if len(res) >= 9:
                    json_load('Safaribet', 'kenya', res[1], day[5], tm[0], tm[1], res[3], res[2], res[7], res[5], res[6])
                match += 1
            except Exception as e:
                run_match = False

    def site_52(self):
        driver.get('https://m.hollywoodbets.net/Menu/Betting/TodaySportCoupon.aspx?sn=Soccer&s=1&bti=15')
        time.sleep(3)
        path = '//*[@id="ctl00_MainContent_couponTable"]/tbody/tr[{}]'
        div, run = 2, True
        while run:
            try:
                gr = driver.find_element(by.XPATH, path.format(div - 1) + '/td/table/tbody/tr[1]').text
                lg = str(driver.find_element(by.XPATH, path.format(div - 1) + '/td/table/tbody/tr[2]/'
                                                                              'td[1]').text).strip(' ')
                t_m = str(driver.find_element(by.XPATH, path.format(div - 1) + '/td/table/tbody/tr[2]/'
                                                                               'td[2]').text).split(' ')
                res = str(driver.find_element(by.XPATH, path.format(div)).text).split('\n')
                json_load('Hollywoodbets', 'zambia', t_m[3], t_m[0], res[0], res[4], lg, gr, res[5], res[1], res[3])
                div += 2
            except Exception as e:
                create_log('Hollywoodbets: zambia', 'getting_data: {}'.format(e), 'error')
                run = False

    def site_55(self):
        driver.get('https://www.gbets.co.za/soccer')
        path = '//*[@id="g-live-events-body"]/table[{}]'
        div, run = 2, True
        while run:
            try:
                elem = driver.find_element(by.XPATH, path.format(div))
                res = str(elem.text).split('\n')
                if res[0] == '':
                    div += 1
                    continue
                if '1 x 2' in res[0]:
                    break
                t_m = elem.find_element(by.XPATH, 'tbody/tr/td[1]').text
                tm = str(elem.find_element(by.XPATH, 'tbody/tr/td[2]/a').text).split(' vs ')
                odds = [elem.find_element(by.XPATH, 'tbody/tr/td[{}]'.format(i)).text for i in range(3, 6)]
                json_load('Hollywoodbets', 'zambia', t_m, day[5], tm[0], tm[1], None, None, odds[2], odds[0], odds[1])
                div += 1
            except Exception as e:
                create_log('Gbets: zambia', 'getting_data: {}'.format(e), 'error')
                run = False


def json_load(bookmaker, country, t_m, date, h_team, a_team, league, s_group, odd_2_val, odd_1_val, odd_x_val):
    t_m = str(t_m) + loc_t
    h_team, a_team = h_team.lower(), a_team.lower()
    odd_1_val = str(odd_1_val)
    odd_2_val = str(odd_2_val)
    odd_x_val = str(odd_x_val)
    with open('{}data_{}-{}-{}.json'.format(config[0], day[1], day[3], day[5]), 'w', encoding='utf-8') as w:
        j_res.append({"country": country, "_entityName": "betwise$Odd",  "odd_1_value": odd_1_val,
                      "odd_x_value": odd_x_val, "odd_2_value": odd_2_val,
                      "bookmaker": {"_entityName": "betwise$Bookmaker", "name": bookmaker},
                      "event": {"_entityName": "betwise$Event",
                                "eventDatetime": "{}-{}-{} {}:00.000".format(day[1], day[3], date, t_m),
                                "homeTeam": h_team, "awayTeam": a_team, "league": league,
                                "superGroup": s_group, "eventType": "Soccer"}})
        json.dump(j_res, w, ensure_ascii=False, indent=2, sort_keys=False)
        data = "{}  {}  {}  {}  {}  {}  {}  {}  {}  {} vs {}".format(bookmaker, date, country, t_m, odd_1_val,
                                                                     odd_x_val, odd_2_val, s_group, league,
                                                                     h_team, a_team)
        create_log('{}: {}'.format(bookmaker, country), 'data: {}'.format(data), 'info')
        print(data)


def read_conf():
    conf = []
    with open('config.cfg', 'r', encoding='utf-8') as r:
        for i in r.read().split('\n'):
            if i != '':
                if i[0] != '#' and len(i) > 6:
                    conf.append(i.split(' =')[1])

    if conf[0] == ' ' or conf[0] == '':
        conf[0] = 'data\\'
    else:
        if conf[0][len(conf[0]) - 1] != '\\':
            conf[0] += '\\'
        if conf[0][0] == ' ':
            conf[0] = conf[0].split(' ', 1)[1]

    conf[1] = conf[1].replace(' ', '')
    if conf[1] == '':
        conf[1] = eval('range(1, 51)')

    return conf[0], conf[1]


def set_day():
    d = str(time.localtime()).replace(',', '=').split('=')
    if len(d[3]) == 1:
        d[3] = '0' + d[3]
    if len(d[5]) == 1:
        d[5] = '0' + d[5]
    return d


def read_data(path, date):
    try:
        with open('{}data_{}-{}-{}.json'.format(path, date[1], date[3], date[5]), 'r', encoding='utf-8') as r:
            j = (json.load(r))
    except:
        open('{}data_{}-{}-{}.json'.format(path, date[1], date[3], date[5]), 'w', encoding='utf-8').write('')
        j = []
    return j


def run_parse(sites):
    if sites != eval('range(1, 51)'):
        sites = sites.split(',')
    for i in sites:
        exc = 0
        while True:
            try:
                exec('Main().site_{}()'.format(int(i)))
                print('================================')
                break
            except Exception as e:
                if 'target window already closed' in str(e):
                    global driver
                    driver = webdriver.Chrome(executable_path=driver_path)
                    driver.maximize_window()
                create_log('Global', 'Starting_parse site_{}: {}'.format(i, e), 'error')
                exc += 1
                time.sleep(1)
                if exc == 3:
                    break
    return None


class DataProcessing(object):
    def __init__(self, file_name):
        self.file_name = file_name
        self.data = None
        self.history = list()
        self.res = list()
        self.log = dict()

    def __read(self):
        with open('data/{}'.format(self.file_name, 'r', encoding='utf-8')) as f:
            self.data = json.load(f)

    def __processing(self):
        dates = dict()
        for event in self.data:
            date = re.findall(r' (\d\d?:\d\d?)\+', event['event']['eventDatetime'])[0]
            if date in dates:
                dates[date] += [event]
            else:
                dates[date] = [event]
        for events in dates.values():
            buffer = dict()
            for event in events:
                team = (event['event']['homeTeam'] + ' v|s ' + event['event']['awayTeam']).lower()
                if len(buffer) == 0:
                    buffer[team] = [event]
                else:
                    added = 0
                    for elem in buffer.keys():
                        seq = difflib.SequenceMatcher(a=team, b=elem).ratio()
                        if seq > 0.65:
                            buffer[elem].append(event)
                            break
                    if added == 0:
                        buffer[team] = [event]
            for key, item in buffer.items():
                accept = 0
                if len(self.history) == 0:
                    self.history.append(key)
                    accept = 1
                elif key in self.history:
                    accept = 1
                else:
                    for i in self.history:
                        if difflib.SequenceMatcher(a=key, b=i).ratio() > 0.65:

                            key = i
                            accept = 1
                            break
                if accept == 0:
                    self.history.append(key)
                home, away = str(key).split(' v|s ')
                for elem in item:
                    self.__logging(elem, home, away)
                    elem['event']['homeTeam'] = home
                    elem['event']['awayTeam'] = away
                    self.res.append(elem)

    def __logging(self, elem, home, away):
        if str(elem['event']['homeTeam']).lower() != str(home).lower():
            if home not in self.log.keys():
                self.log[home] = [str(elem['event']['homeTeam']).lower()]
            elif str(elem['event']['homeTeam']).lower() not in self.log[home]:
                self.log[home].append(str(elem['event']['homeTeam']).lower())
        if str(elem['event']['awayTeam']).lower() != str(away).lower():
            if away not in self.log.keys():
                self.log[away] = [str(elem['event']['awayTeam']).lower()]
            elif str(elem['event']['awayTeam']).lower() not in self.log[away]:
                self.log[away].append(str(elem['event']['awayTeam']).lower())

    def __save(self):
        name, _ = str(self.file_name).split('.')
        with open('data/{}'.format(self.file_name), 'w', encoding='utf-8') as f:
            json.dump(self.res, f, ensure_ascii=False, indent=2, sort_keys=False)
        with open('logs/{}_name_changes_log.json'.format(name), 'w', encoding='utf-8') as f:
            json.dump(self.log, f, ensure_ascii=False, indent=1, sort_keys=False)

    def run(self):
        self.__read()
        self.__processing()
        self.__save()


if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s - %(asctime)s - %(message)s',
                        filename='logs\\execution.log', level=logging.INFO)
    logging.basicConfig(format='%(levelname)s - %(asctime)s - %(message)s',
                        filename='logs\\execution.log', level=logging.ERROR)

    driver_path = "{}\\chromedriver.exe".format(os.getcwd())
    loc_t = '+' + str(datetime.timezone(datetime.timedelta(seconds=-time.timezone))).split('+')[1] + ':00.000'
    config = read_conf()
    day = set_day()
    j_res = read_data(config[0], day)
    driver = webdriver.Chrome(executable_path=driver_path)
    driver.maximize_window()
    run_parse(config[1])
    print('============================================================\n')
    print('Complete Parsing\n')
    driver.close()
    print('Start Data Improvement...')
    DataProcessing('data_{}-{}-{}.json'.format(day[1], day[3], day[5])).run()

