from selenium import webdriver
from selenium.webdriver.common.by import By as by
import telebot
import json
import time

TOKEN = '460840710:AAHuamu7SYZ4ziNZYiqi57tzF06ysBJlxrE'
CHAT_ID = '160037475'
FC_LOGIN = 'Artur11735'
FC_PASS = 'ytrewq1qaz'


def login():
    try:
        path = '/html/body/div[1]/div[2]/div/div/div/div/div[2]/form/table/tbody/'
        driver.get('https://www.facebook.com')
        driver.find_element(by.XPATH, path + 'tr[2]/td[1]/input').send_keys('Artur11735')
        driver.find_element(by.XPATH, path + 'tr[2]/td[2]/input').send_keys('ytrewq1qaz')
        driver.find_element(by.XPATH, path + 'tr[2]/td[3]').click()
    except:
        path = '//*[@id="u_0_9"]/'
        driver.find_element(by.XPATH, path + 'div[2]').send_keys('Artur11735')
        driver.find_element(by.XPATH, path + 'div[3]').send_keys('ytrewq1qaz')
        driver.find_element(by.XPATH, path + 'div[4]').click()

    driver.get('https://www.freelancer.com/login')
    path = '/html/body/div[1]/main/fl-login-signup-angular/fl-login-signup-modal/div/div/div/div/fl-login/'
    driver.find_element(by.XPATH, path + 'fl-login-form/div[2]/form[1]/button').click()
    time.sleep(5)
    return 0


def scrape():
    driver.get('https://www.freelancer.com/search/projects/')
    time.sleep(3)
    path = '/html/body/div[4]/div[2]/div[1]/div[10]/'
    page = 4
    li = 1
    while True:
        try:
            a = driver.find_element(by.XPATH, '//*[@id="search-results"]/ul/li[{}]/a'.format(li)).get_attribute('href')
            driver.get('{}/'.format(a))
            try:
                id = driver.find_element(by.XPATH, path + 'div[8]/div[2]/div/span[2]').text
            except:
                id = driver.find_element(by.XPATH, path + 'div[9]/div[2]/div/span[2]').text
            if id in j_res:
                driver.back()
                page = 3
                try:
                    driver.find_element(by.XPATH, '//*[@id="search-results"]/div[4]/ul/li[3]/a').click()
                    time.sleep(5)
                except:
                    time.sleep(60)
                    pass
                li = 1
                continue
            h1 = driver.find_element(by.XPATH, path + 'div[2]/div[3]/div/div[1]/h1').text
            try:
                info = str(driver.find_element(by.XPATH, path + 'div[3]/div[5]').text).split('\n')
            except:
                info = str(driver.find_element(by.XPATH, path + 'div[4]/div[5]').text).split('\n')
            desc = str(driver.find_element(by.XPATH, path + 'div[8]/div[1]/div[1]').text).split('About the employer')[0]
            json_load(id, h1, info, desc)
            driver.back()
            time.sleep(3)
            li += 1
        except:
            driver.back()
            if page == 6:
                page = 3
            driver.find_element(by.XPATH, '//*[@id="search-results"]/div[4]/ul/li[{}]/a'.format(page)).click()
            page += 1


def json_load(key, h1, info, desc):
    with open('data.json', 'w', encoding='utf-8') as w:
        j_res[key] = h1
        json.dump(j_res, w, ensure_ascii=False, indent=2, sort_keys=False)
    message = '{} \n\n Bids: {} \n {}  {} \n\n {}'.format(h1, info[1], info[4], info[5], desc)
    bot = telebot.TeleBot(TOKEN)
    bot.send_message(CHAT_ID, message)


if __name__ == '__main__':
    with open('data.json', 'r', encoding='utf-8') as r:
        j_res = json.load(r)
    driver = webdriver.Firefox()
    login()
    scrape()
