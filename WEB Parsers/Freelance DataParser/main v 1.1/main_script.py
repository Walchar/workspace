from telegram.ext import Updater
from selenium import webdriver
from selenium.webdriver.common.by import By as by
from requests_.silenium.Freelance.config import *
import time
from googletrans import Translator


def add_info(info):
    with open("telegram_info.csv", "a", encoding='utf-8') as f:
        f.write('{}\n'.format(info))
        f.close()
    return


def add_id(id):
    print('add_id')
    with open("projects_id.csv", "a", encoding='utf-8') as f:
        f.write('{}\n'.format(id))
        f.close()
    return


def add_link_info():
    print('add_link_info')
    count = 2
    all_descrip = ''
    budget = driver.find_element(by.XPATH, '/html/body/div[4]/div[2]/div'
                                           '[1]/div[10]/div[3]/div[5]/div[3]/p').text + '\n'
    cost = driver.find_element(by.XPATH, '/html/body/div[4]/div[2]/div'
                                         '[1]/div[10]/div[3]/div[5]/div[3]/div').text + '\n'
    try:
        while True:
            link_info = driver.find_element(by.XPATH, '/html/body/div[4]/div[2]/div[1]'
                                                      '/div[10]/div[8]/div[1]/div[1]/p[{}]'.format(count)).text
            all_descrip += link_info + '\n'
            count += 1
    except:
        print('Translating...')
        translator = Translator()
        buffer = translator.translate(all_descrip, dest='ru', src='en')
        buffer = str(buffer).replace('Translated(src=en, dest=ru, text=', 'Translated:\n')
        res = buffer.split(', pronunciation=')
        add_info('\n' + budget + cost + '\nProject Description:\n' + '{}'.format(all_descrip) + res[0])
        return


def send_message():
    print('send_message')
    with open("telegram_info.csv", "r", encoding='utf-8') as f:
        text = f.read()
        updater = Updater(token)
        updater.bot.send_message(chat_id=int(chat_id), text=text)
        f.close()


def sleep():
    print('sleep 60sec')
    count = 60
    for i in range(60):
        time.sleep(1)
        count -= 1
        nul = ''
        if count < 10:
            nul = '0'
        if count % 10 == 0:
            print(' 00:{}{} seconds left'.format(nul, count))


def login():
    driver.get('https://www.facebook.com/login.php?login_attempt=1&lwv=110')
    time.sleep(1)
    face_log = driver.find_element(by.XPATH, '//*[@id="email"]')
    face_log.send_keys(facebook_login)
    face_pas = driver.find_element(by.XPATH, '//*[@id="pass"]')
    face_pas.send_keys(facebook_password)
    driver.find_element(by.CSS_SELECTOR, 'div._xkt:nth-child(16)').click()
    while True:
        try:
            driver.get('https://www.freelancer.com/login')
            time.sleep(1)
            driver.find_element(by.CSS_SELECTOR, '#fb-login-btn').click()
            time.sleep(5)
            break
        except:
            continue
    return


def get_info():
    tr_num = 1
    while True:
        try:
            with open("telegram_info.csv", "w", encoding='utf-8') as n_f:
                n_f.write('')
                n_f.close()
            if tr_num > 5:
                tr_num = 1
            print('get_info_while_try \n' +
                  'https://www.freelancer.com/jobs/myskills/\n' +
                  'PROJECT NUMBER:{}'.format(tr_num))
            driver.get('https://www.freelancer.com/jobs/myskills/')
            time.sleep(1)
            driver.find_element(by.XPATH, '/html/body/div[6]/div/div/div[1]/div/div/div[1]'
                                          '/section/div[7]/div[1]/div[2]/div[1]/select/option[2]').click()
            id = driver.find_element(by.XPATH, '/html/body/div[6]/div/div/div[1]/div/'
                                               'div/div[2]/div/table/tbody/tr[{}]'.format(tr_num))
            res_id = id.get_attribute('id')
            file = open('projects_id.csv', 'r')
            text = file.read()
            file.close()
            if id.get_attribute('id') in text:
                tr_num = 1
                sleep()
                continue
            else:
                add_info(id.get_attribute('id'))
            link = driver.find_element(by.XPATH, '/html/body/div[6]/div/div/div[1]/div/div/'
                                                 'div[2]/div/table/tbody/tr[{}]/td[1]/h2/a'.format(tr_num))
            res_link = link.get_attribute('href')
            add_info(link.get_attribute('href'))
            link.click()
            add_link_info()
            print('go to send_message')
            send_message()
            add_id(res_id + '  {}'.format(res_link))
            tr_num += 1
        except:
            print('ERROR')


if __name__ == '__main__':
    with open("projects_id.csv", "w", encoding='utf-8') as n_f:
        n_f.write('')
        n_f.close()
    driver = webdriver.Firefox()
    login()
    get_info()